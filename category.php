<?php get_header(); ?>
<div class="col-12 col-lg-8 col-xl-8">
  
  <?php dynamic_sidebar( 'ads-72890' ); ?>

  <div id="recent-apps">
    <div class="card mb-3">
      <h5 class="widget-title">
        <?php 
          $obj = get_queried_object();
          printf( __( 'All %s apps', 'apk' ), single_term_title('',false) );
          $applications = get_terms( array(
            'taxonomy'    => 'appcategory',
            'number'      => -1,
            'meta_query'  => array(
              array(
                'key'     => 'appcat_id',
                'value'   => $obj->term_id,
                'compare' => '='
              )
            )
          )
        );
      ?>
      </h5>
      <?php
      if ( !empty( $applications ) ) {
        echo '<div class="app-table">';
        foreach( $applications as $app ) { 
          $icon_img = '';
          $app_icon = get_term_meta( $app->term_id, 'app_icon_id', true );
          if ( $app_icon ) {
            $icon_img = wp_get_attachment_image( $app_icon, array(32,32), true );
          }
        ?>
        <div class="app-apk app-row">
          <div class="icon" style="width: 56px"><?php echo $icon_img; ?></div>
          <div class="app-name app-title">
            <h5 class="title"><a href="<?php echo get_term_link( $app ); ?>"><?php echo $app->name; ?></a></h5>
          </div>
          <div class="app-info-download">
            <a href="<?php echo get_term_link( $app ); ?>"><i class="material-icons">file_download</i></a>
          </div>
        </div>
        <?php }
        echo '</div>';
      } ?>
    </div>
  </div>
</div>
<?php get_sidebar(); ?>
<?php get_footer(); ?>