<?php get_header(); 
  while ( have_posts() ) : the_post();
  $post     = get_queried_object();
  $release  = get_post_meta( $post->ID, 'release', true );
  $taxonomy = 'appcategory';
  $cat      = get_the_category( $release[0] );
  $obj      = get_the_terms( $release[0], $taxonomy );
  if ( ! is_wp_error( $obj ) && !empty( $obj ) ) {
    $obj = $obj[0];
  }
?>

<div class="col-12 col-lg-8 col-xl-8">

  <?php dynamic_sidebar( 'ads-72890' ); ?>
  
  <div id="app-tabs">
    <div class="card-with-tabs mb-3">

      <div class="tab-container">
        <div class="tab-scrollable-container">
          <ul class="nav nav-tabs" id="myTab" role="tablist">
            <li class="nav-item">
              <a class="nav-link active" data-toggle="tab" href="#file-tab" role="tab" aria-controls="file-tab" aria-selected="true"><?php _e( 'File', 'apk' ); ?></a>
            </li>
            <li class="nav-item">
              <a class="nav-link" data-toggle="tab" href="#changelog-tab" role="tab" aria-controls="changelog-tab" aria-selected="false"><?php _e( 'What\'s New', 'apk' ); ?></a>
            </li>
            <li class="nav-item">
              <a class="nav-link" data-toggle="tab" href="#description-tab" role="tab" aria-controls="description-tab" aria-selected="true"><?php _e( 'Descriptions', 'apk' ); ?></a>
            </li>
            <li class="nav-item">
              <a class="nav-link" data-toggle="tab" href="#variants-tab" role="tab" aria-controls="variants-tab" aria-selected="true"><?php _e( 'All Variants', 'apk' ); ?></a>
            </li>
          </ul>
        </div>
      </div>
      <!-- end .tab-container -->

      <div class="tab-buttons d-flex align-items-center justify-content-between">
        <div class="app-meta">
          <div class="app-category">
            <?php if ( $cat ) {
              printf('<a href="%s">%s</a>', get_term_link( $cat[0] ), $cat[0]->name ); 
            }; ?>
          </div>
          <div class="app-rating d-flex align-items-center">
            <?php ms_rating( $obj->term_id ); ?>
            <span><?php printf( __( '(%s ratings)', 'apk' ), number_format( (float) get_term_meta( $obj->term_id, 'total_rating', true ) ) ); ?></span>
          </div>
        </div>
        <?php include( locate_template( 'parts/sharer.php' ) ); ?>
      </div>

      <div class="tab-content" id="myTabContent">
        <div class="tab-pane fade show active" id="file-tab" role="tabpanel" aria-labelledby="file-tab">
          <div class="row">
            <div class="col-md-6 col-sm-6 col-xs-12">
              <?php include( locate_template( 'parts/apk-spec.php' ) ); ?>
            </div>
            <div class="col-md-6 col-sm-6 col-xs-12">
              <?php dynamic_sidebar( 'ads-download_1-330' ); ?>
              <a class="download-apk-btn btn btn-raised btn-primary btn-block" href="<?php echo get_theme_file_uri( '/download.php?id='.get_the_ID() ); ?>"><i class="material-icons">file_download</i><?php _e( 'Download APK', 'apk' ); ?></a>
            </div>
          </div>
        </div>
        <div class="tab-pane fade" id="changelog-tab" role="tabpanel" aria-labelledby="changelog-tab">
          <div class="entry-content">
            <?php $qquery = new WP_Query( array(
              'post_type' => 'app_release',
              'posts_per_page' => 1,
              'post__in' => get_post_meta( get_the_ID(), 'release', true )
            ) ); ?>
            <?php if ( $qquery->have_posts() ) : while ( $qquery->have_posts() ) : $qquery->the_post(); ?>
              <?php the_content(); ?>
            <?php endwhile; endif; wp_reset_postdata(); ?>
          </div>
        </div>
        <div class="tab-pane fade" id="description-tab" role="tabpanel" aria-labelledby="description-tab">
          <?php 
            $string = $obj->description;
            // $email  = '/(\S+@\S+\.\S+)/';
            // $string = preg_replace($email, '<a href="mailto:$1">$1</a>', $string );
            // $url    = '@(http(s)?)?(://)?(([a-zA-Z])([-\w]+\.)+([^\s\.]+[^\s]*)+[^,.\s])@';
            // $string = preg_replace($url, '<a href="http$2://$4" target="_blank" title="$0">$0</a>', $string );
            echo wpautop( make_clickable( $string ) ); 
          ?>
        </div>
        <div class="tab-pane fade" id="variants-tab" role="tabpanel" aria-labelledby="variants-tab">
          <?php include( locate_template( 'parts/all-variants.php' ) ); ?>
        </div>
      </div>
    </div><!-- end .card -->   
  </div><!-- end #recent-apps -->

  <?php dynamic_sidebar( 'ads-download_2-72890' ); ?>

  <div id="downloads">
    <div class="card mb-3">
      <h5 class="widget-title date"><?php printf( __( 'Previous APKs for (%s) variant', 'apk' ), ms_get_apk_meta( 'architecture' ) ); ?></h5>
      <?php
        $r_ids = array();
        $args  = array(
          'post_type' => array( 'app_release' ),
          'posts_per_page' => 10,
          'tax_query' => array(
            array(
              'taxonomy' => 'appcategory',
              'terms' => array( $obj->term_id )
            )
          )
        );
        $relquery = new WP_Query( $args );
        if ( $relquery->have_posts() ) : ?>
          <?php while ( $relquery->have_posts() ) : $relquery->the_post(); $r_ids[] = get_the_ID(); ?>
          <?php endwhile; ?>
      <?php endif; wp_reset_postdata(); ?>

      <?php if ( !empty( $r_ids ) ) {
        foreach( $r_ids as $r_id ) {
          $download = new WP_Query( array(
            'post_type' => array( 'app_download' ),
            'posts_per_page' => 10,
            'meta_query' => array(
              array(
                'key' => 'release',
                'value' => $r_id,
                'compare' => 'LIKE'
              ),
              array(
              'key'     => 'apk_info_architecture',
                'compare' => 'LIKE',
                'value'   => get_post_meta( $post->ID, 'apk_info_architecture', true )
              )
            )
          ) );
          if ( $download-> have_posts() ) {
            while ( $download->have_posts() ) : $download->the_post(); 
               get_template_part( 'parts/content-download', 'loop' );
            endwhile;
          }; wp_reset_postdata();
        }
      } ?>
    </div><!-- end .card -->
  </div>
  
  <?php dynamic_sidebar( 'ads-download_3-72890' ); ?>

  <div id="all-releases">
    <div class="card mb-3">
      <h5 class="widget-title date"><?php _e( 'All Versions', 'apk'); ?></h5>
      <?php
        $args  = array(
          'post_type' => array( 'app_release' ),
          'date_pagination_type' => 'daily',
          'posts_per_page' => 10,
          'tax_query' => array(
            array(
              'taxonomy' => 'appcategory',
              'terms' => array( $obj->term_id )
            )
          )
        );
        $asquery = new WP_Query( $args );
        if ( $asquery->have_posts() ) : ?>
          <?php while ( $asquery->have_posts() ) : $asquery->the_post(); ?>
            <?php get_template_part( 'parts/content-main', 'loop' ); ?>
          <?php endwhile; ?>
      <?php endif; wp_reset_postdata(); ?>
    </div><!-- end .card -->
  </div>

    <div class="card mt-3">
    <h5 class="widget-title date"><?php _e( 'Comments', 'apk' ); ?></h5>
    <div class="disqus-comments-wraper">
      <?php comments_template(); ?>
    </div>
  </div>

</div><!-- end col -->

<?php endwhile; ?>

<?php get_sidebar(); ?>

<?php get_footer(); ?>