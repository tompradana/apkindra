<?php
$tax = 'devcategory';
$dev = get_the_terms( get_the_ID(), $tax );
if ( is_wp_error(  $dev ) || (is_array( $dev ) && empty( $dev ) ) ) {
	$dev = '';
}; 
$icon_img = '';
$app_cats = get_the_terms( get_the_ID(), 'appcategory' );
if ( $app_cats ) {
	$app_icon = get_term_meta( $app_cats[0]->term_id, 'app_icon_id', true );
	if ( $app_icon ) {
		$icon_img = wp_get_attachment_image( $app_icon, array(32,32), true );
	}
}
$rilisid = get_the_ID();
?>
<div class="app-table">
	<div class="app-apk app-row">
		<?php if ( $icon_img ) { ?>
			<div class="icon" style="width: 56px"><?php echo $icon_img; ?></div>
		<?php } ?>
		<div class="app-name app-title">
			<h5 title="<?php the_title(); ?>"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h5><?php if ( $dev && !is_tax() && !is_singular( array( 'app_release', 'app_download' ) ) ) { ?><a class="by-developer" href="<?php echo get_term_link( $dev[0], $tax ); ?>">by <?php echo $dev[0]->name; ?></a><?php } ?>
			<?php ms_get_apk_files_counts( get_the_ID() ); ?>
			<a class="dateyear_utc" data-utcdate="<?php echo get_gmt_from_date( get_the_date(), 'd/m/Y H:i' ); ?>"><?php echo get_gmt_from_date( get_the_date(), 'j F Y' ); ?></a>
		</div>
		<div class="app-date">
			<a class="dateyear_utc" data-utcdate="<?php echo get_gmt_from_date( get_the_date(), 'd/m/Y H:i' ); ?>"><?php echo get_gmt_from_date( get_the_date(), 'j F Y' ); ?></a>
		</div>
		<div class="app-info-download">
			<a class="view-detail" href="#"><i class="material-icons">info</i></a>
			<a href="<?php the_permalink(); ?>"><i class="material-icons">file_download</i></a>
		</div>
	</div>
</div>
<div class="infoSlide">
	<p><span class="infoslide-name">Version</span><span class="infoslide-value"><?php the_field( 'version' ); ?></span></p>
	<p><span class="infoslide-name">Uploaded</span><span class="infoslide-value"><?php if ( is_page_template( 'page-templates/homepage.php' ) ) : ?><?php echo esc_html( human_time_diff( get_the_time('U'), current_time('timestamp') ) ) . ' ago'; ?><?php else : ?><?php echo printf( __( '%s at %s', 'apk' ), get_the_date( 'F j, Y '), get_the_date( 'g:iA T' ) ); ?><?php endif; ?></span>
	</p>
	<p><span class="infoslide-name">File Size</span><span class="infoslide-value"><?php echo ms_get_file_size( get_the_ID() ); ?></span></p>
	<p><span class="infoslide-name">Downloads</span><span class="infoslide-value"><?php echo ms_get_download_count(get_the_ID() ); ?></span></p>
</div>