<div class="apk-downloads">
  <div class="table-responsive">
    <table class="table">
      <thead>
        <tr>
          <th scope="col">
            <?php _e( 'Variant', 'apk' ); ?>
          </th>
          <th scope="col">
            <?php _e( 'Architecture', 'apk' ); ?>
          </th>
          <th scope="col">
            <?php _e( 'Minimum Version', 'apk' ); ?>
          </th>
          <th scope="col">
            <?php _e( 'Screen DPI', 'apk' ); ?>
          </th>
        </tr>
      </thead>
      <tbody>
        <?php
        $release_id = get_post_meta( get_the_ID(), 'release', true );
                $args  = array(
                  'post_type'       => 'app_download',
                  'posts_per_page'  => -1,
                  'order'           => 'ASC',
                  'meta_key'        => 'release',
                  'meta_value'      => $release_id[0],
                  'meta_compare'    => 'LIKE'
                );
                $query = new WP_Query( $args );
                if ( $query->have_posts() ) : ?>
        <?php while ( $query->have_posts() ) : $query->the_post(); ?>
        <tr>
          <th scope="row"><a href="<?php the_permalink(); ?>"><i class="material-icons">label</i>
              <?php echo ms_get_apk_meta( 'variant' ); ?></a></th>
          <td>
            <?php echo ms_get_apk_meta( 'architecture' ); ?>
          </td>
          <td>
            <?php echo ms_get_apk_meta( 'min_version' ); ?>
          </td>
          <td>
            <?php echo ms_get_apk_meta( 'screen' ); ?>
          </td>
        </tr>
        <?php endwhile; ?>
        <?php endif; wp_reset_postdata(); ?>
      </tbody>
    </table>
  </div>
</div>