<?php global $wp; ?>
<div class="app-sharer d-flex align-items-center">
  <div class="qr dropdown">
  	<a href="#" role="button"data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fas fa-qrcode"></i></a>
  	<div class="dropdown-menu">
	   	<?php $url_item = urlencode( home_url( '/'. $wp->request ) ); ?>
	   	<img src="https://api.qrserver.com/v1/create-qr-code/?size=150x150&data=<?php echo $url_item; ?>">
	  </div>
  </div>
  <div class="share dropdown">
  	<a href="#" role="button"data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fas fa-share-alt"></i></a>
  	<div class="dropdown-menu">
	   	<?php $url_item = urlencode( home_url( '/'. $wp->request ) ); ?>
	   	<a target="_blank" class="dropdown-item" href="https://www.facebook.com/sharer/sharer.php?u=<?php echo $url_item; ?>">Share on Facebook</a>
	   	<a target="_blank" class="dropdown-item" href="https://twitter.com/home?status=<?php echo $url_item; ?>">Share on Twitter</a>
	  </div>
  </div>
  <div class="play"><a href="<?php echo $play_url = get_term_meta( $obj->term_id, 'play_url', true ); ?>" target="_blank"><i class="fab fa-google-play"></i></a></div>
  <div class="rss"><i class="fas fa-rss"></i></div>
</div>