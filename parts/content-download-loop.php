<?php
$tax = 'devcategory';
$dev = get_the_terms( get_the_ID(), $tax );
if ( is_wp_error(  $dev ) || (is_array( $dev ) && empty( $dev ) ) ) {
  $dev = '';
}; 
$icon_img = '';
$release  = get_post_meta( get_the_ID(), 'release', true );
$app_cats = get_the_terms( $release[0], 'appcategory' );
if ( $app_cats ) {
	$app_icon = get_term_meta( $app_cats[0]->term_id, 'app_icon_id', true );
	if ( $app_icon ) {
		$icon_img = wp_get_attachment_image( $app_icon, array(32,32), true );
	}
}
?>
<div class="app-table">
<div class="app-apk app-row">
  <?php if ( $icon_img ) { ?>
  	<div class="icon" style="width: 56px"><?php echo $icon_img; ?></div>
  <?php } ?>
  <div class="app-name app-title">
    <?php printf(
      '<h5 title="%1$s"><a href="%2$s">%1$s (%3$s)</a></h5>',
      get_the_title(),
      get_permalink(),
      get_post_meta( get_the_ID(), 'apk_info_architecture', true )
    ); ?>
  </div>
  <div class="app-info-download">
    <!-- <span class="date"><?php echo get_the_date( 'F j, Y' ); ?></span> -->
    <a class="view-detail" href="#"><i class="material-icons">info</i></a>
    <a href="<?php the_permalink(); ?>"><i class="material-icons">file_download</i></a>
  </div>
</div>
</div>
<div class="infoSlide">
  <?php $download = '' != get_post_meta( get_the_ID(), '_download_count', true ) ? get_post_meta( get_the_ID(), '_download_count', true ): '0'; ?>
  <p><span class="infoslide-name">Version</span><span class="infoslide-value"><?php echo ms_get_apk_meta( 'version' ); ?> (<?php echo ms_get_apk_meta( 'min_version' ); ?>)</span></p>
  <p><span class="infoslide-name">Uploaded</span><span class="infoslide-value"><?php echo printf( __( '%s at %s', 'apk' ), get_the_date( 'F j, Y '), get_the_date( 'g:iA T' ) ); ?></span></p>
  <p><span class="infoslide-name">File Size</span><span class="infoslide-value"><?php echo ms_get_apk_meta( 'filesize' ); ?> MB</span></p>
  <p><span class="infoslide-name">Downloads</span><span class="infoslide-value"><?php echo $download; ?></span></p>
</div>