<?php
$tax = 'devcategory';
$dev = get_the_terms( get_the_ID(), $tax );
if ( is_wp_error(  $dev ) || (is_array( $dev ) && empty( $dev ) ) ) {
  $dev = '';
}; 
$icon_img = '';
$app_cats = get_the_terms( get_the_ID(), 'appcategory' );
if ( $app_cats ) {
	$app_icon = get_term_meta( $app_cats[0]->term_id, 'app_icon_id', true );
	if ( $app_icon ) {
		$icon_img = wp_get_attachment_image( $app_icon, array(32,32), true );
	}
}
?>
<div class="app-table">
<div class="app-apk app-row">
  <?php if ( $icon_img ) { ?>
  	<div class="icon" style="width: 56px"><?php echo $icon_img; ?></div>
  <?php } ?>
  <div class="app-name app-title">
<h5 title="<?php the_title(); ?>"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h5><?php if ( $dev && !is_tax() && !is_singular( array( 'app_release', 'app_download' ) ) ) { ?><a class="by-developer" href="<?php echo get_term_link( $dev[0], $tax ); ?>">by <?php echo $dev[0]->name; ?></a><?php } ?>
  </div>
  <div class="download-count">
    <span><?php echo ms_get_download_count(get_the_ID()); ?></span>
  </div>
  <div class="app-info-download">
    <a href="<?php the_permalink(); ?>"><i class="material-icons">file_download</i></a>
  </div>
</div>
</div>