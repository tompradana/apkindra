<div class="apkspec">
	<div class="specrow">
		<div class="specicon"><span class="material-icons">developer_board</span></div>
		<div class="specvalue">
			<?php printf( __('Version: %s (%s)', 'apk' ), ms_get_apk_meta( 'version' ), ms_get_apk_meta( 'variant' ) ); ?><br />
			<?php if (''<>ms_get_apk_meta( 'architecture' )) : ?><?php echo ms_get_apk_meta( 'architecture' ); ?><br /><?php endif; ?>
			<?php printf( __( 'Package: %s', 'apk' ), str_replace( 'id=', '', parse_url( $play_url, PHP_URL_QUERY ) ) ); ?>
		</div>
	</div>
	<!-- end row -->

	<div class="specrow">
		<div class="specicon"><span class="material-icons">dns</span></div>
		<div class="specvalue">
			<?php 
			$mb = ms_get_apk_meta( 'filesize' ); 
			if ( $mb ) {
				echo $mb . ' MB ';
				$bytes = ms_convert_to_byte( $mb . 'MB' );
				if ( $bytes ) {
					printf( __( '(%s bytes)', 'apk' ), number_format( $bytes ) );
				}
			}
			?>
		</div>
	</div>

	<div class="specrow">
		<div class="specicon"><span class="material-icons">android</span></div>
		<div class="specvalue">
			<?php echo ms_get_apk_meta( 'android_version' ); ?>
		</div>
	</div>

	<div class="specrow">
		<div class="specicon"><span class="material-icons">stay_primary_portrait</span></div>
		<div class="specvalue">
			<?php echo ms_get_apk_meta( 'screen' ); ?>
		</div>
	</div>

	<?php if ( '' <> ms_get_apk_meta( 'signatures' ) ) : ?>
		<div class="specrow">
			<div class="specicon"><span class="material-icons">check_circle</span></div>
			<div class="specvalue">
				<a href="#" data-toggle="modal" data-target="#hashModal"><?php _e( 'MD5, SHA-1, SHA-256 signatures', 'apk' ); ?></a>
			</div>
		</div>  	
	<?php endif; ?>

	<div class="specrow">
		<div class="specicon"><span class="material-icons">event</span></div>
		<div class="specvalue">
			<?php printf( __( 'Uploaded %s at %s GMT+0700 by %s', 'apk' ), get_the_date( 'F j, Y' ), get_the_time( 'H:iA' ), get_the_author() ); ?>
		</div>
	</div>
</div>

<?php if ( '' <> ms_get_apk_meta( 'signatures' ) ) : ?>
	<!-- Modal -->
	<div class="modal fade" id="hashModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
		<div class="modal-dialog modal-dialog-centered modal-lg" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="exampleModalLongTitle">Safe to Download</h5>
				</div>
				<div class="modal-body">
					<?php echo wpautop( ms_get_apk_meta( 'signatures' ) ); ?>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Got It!</button>
				</div>
			</div>
		</div>
	</div>
	<?php endif; ?>