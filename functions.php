<?php
// env
if ( !defined( 'SQ_ENV' ) ) {
	define( 'SQ_ENV', 'staging' );
}

// setup
function ms_setup_theme() {
	global $themename, $shortname;
	$themename = 'APK Indra';
	$shortname = 'apk';

	$GLOBALS['content_width'] = apply_filters( 'ms_content_width', 767 );

	load_theme_textdomain( $shortname, get_theme_file_path( 'languages' ) );

	add_theme_support( 'automatic-feed-links' );
	add_theme_support( 'title-tag' );
	add_theme_support( 'post-thumbnails' );
	add_theme_support( 'editor-styles' );
	add_theme_support( 'responsive-embeds' );
	add_theme_support( 'html5',array(
		'search-form',
		'comment-form',
		'comment-list',
		'gallery',
		'caption'
	) );

	register_nav_menus( array(
		'main' 		=> __( 'Main Menu', 'apk' ),
		'footer'	=> __( 'Footer Menu', 'apk' ),
		'mobile'	=> __( 'Mobile Menu', 'apk' )
	) );

	remove_theme_support( 'widgets-block-editor' );
}
add_action( 'after_setup_theme', 'ms_setup_theme' );

// includes
include( 'vendor/autoload.php' );
include( 'includes/release-manager/release-manager.php' );
include( 'includes/funct/settings.php' );
include( 'includes/funct/plugins.php' );
include( 'includes/funct/posttypes.php' );
include( 'includes/funct/taxonomies.php' );
include( 'includes/funct/custom.php' );
include( 'includes/funct/widgets.php' );
include( 'includes/widgets/recent-widget.php' );
include( 'includes/widgets/popular-widget.php' );
include( 'includes/widgets/social-widget.php' );

// assets
function ms_enqueue_assets() {
	$ver = '1.0.0';
	if ( defined( 'SQ_ENV' ) && SQ_ENV === 'staging' ) {
		$ver = time();
	}

	wp_enqueue_style( 'fontawesome-icons', '//cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/css/all.min.css' );
	wp_enqueue_style( 'material-icons', '//fonts.googleapis.com/css?family=Roboto:300,400,500,700|Material+Icons' );
	wp_enqueue_style( 'material-bs', '//unpkg.com/bootstrap-material-design@4.1.1/dist/css/bootstrap-material-design.min.css' );
	wp_enqueue_style( 'apk-css', get_theme_file_uri( 'assets/css/main.css' ), null, $ver );
	wp_enqueue_style( 'apk-custom-css', get_stylesheet_uri(), null, $ver );

  	wp_enqueue_script( 'popper', "//unpkg.com/popper.js@1.12.6/dist/umd/popper.js", null, false, true );
	wp_enqueue_script( 'material-bs', '//unpkg.com/bootstrap-material-design@4.1.1/dist/js/bootstrap-material-design.js', array( 'jquery', 'popper' ), '4.0.1', true );
	wp_enqueue_script( 'apk-js', get_theme_file_uri( 'assets/js/functions.js' ), array( 'material-bs' ), $ver, true );
		
	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'ms_enqueue_assets' );