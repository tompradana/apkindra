<?php get_header(); ?>

<div class="col-12 col-lg-8 col-xl-8">

  <?php dynamic_sidebar( 'ads-72890' ); ?>

  <div id="recent-apps">
  	<div class="card mb-3">
      <h5 class="widget-title">
        <?php if ( is_tag() || is_category() ) {
        	$obj = get_queried_object();
        	printf( __( 'All %s apps', 'apk' ), single_term_title('',false) );
        }; ?>
      </h5>
      <?php while ( have_posts() ) : the_post(); ?>
      	<?php get_template_part( 'parts/content', 'loop' ); ?>
      <?php endwhile; ?>
    </div>
  </div>

  <?php dynamic_sidebar( 'ads-72890-bottom' ); ?>

</div>

<?php get_sidebar(); ?>
<?php get_footer(); ?>