<?php get_header(); 
$obj = get_queried_object();
?>

<div class="col-12 col-lg-8 col-xl-8">
	
	<?php dynamic_sidebar( 'ads-72890' ); ?>

	<div id="all-uploads">
		<div class="card mb-3">
			<h5 class="widget-title date"><?php printf( __( 'Latest %s Uploads', 'apk' ), single_term_title( '', false ) ); ?></h5>
			<?php
			$args  = array(
				'post_type'            => array( 'app_release' ),
				'date_pagination_type' => 'daily',
				'posts_per_page'       => 10,
				'tax_query'            => array(
					array(
						'taxonomy' => 'devcategory',
						'terms' => $obj->term_id
					)
				)
			);
			$query = new WP_Query( $args );
			if ( $query->have_posts() ) : ?>
				<?php while ( $query->have_posts() ) : $query->the_post(); ?>
					<?php get_template_part( 'parts/content-devcategory', 'loop' ); ?>
				<?php endwhile; ?>
			<?php endif; wp_reset_postdata(); ?>
		</div><!-- end .card -->
	</div>

	<?php dynamic_sidebar( 'ads-dev-72890' ); ?>

	<div id="all-apps">
		<div class="card mb-3">
			<h5 class="widget-title date"><?php printf( __( 'All %s Apps', 'apk' ), single_term_title( '', false ) ); ?></h5>
			
			<?php
			$apps = get_terms( array(
				'taxonomy' 		=> 'appcategory',
				'meta_key' 		=> 'devcategory_id',
				'meta_value'	=> $obj->term_id
			) );
			?>
			<?php if ( $apps ) : ?>
				<?php foreach( $apps as $app ) : 
					$attachment_id = get_term_meta( $app->term_id, 'app_icon_id', true );
					?>
					<div class="app-table">

						<div class="app-apk app-row">
							<?php if ( $attachment_id ) : ?>
								<div class="icon" style="width: 56px">
									<?php echo wp_get_attachment_image( $attachment_id, array( 32,32 ), true ); ?>
								</div>
							<?php endif; ?>
							<div class="app-name app-title">
								<h5 title="<?php echo $app->name; ?>"><a href="<?php echo get_term_link( $app ); ?>"><?php echo $app->name; ?></a></h5>
							</div>

							<div class="app-info-download">
								<a class="view-detail" href="#"><i class="material-icons">info</i></a>
								<a href="<?php echo get_term_link( $app ); ?>"><i class="material-icons">file_download</i></a>
							</div>
						</div>
					</div>
					<?php
					$args  = array(
						'post_type'            	=> array( 'app_release' ),
						'posts_per_page'       	=> 1,
						'orderby' 				=> 'date',
			            'order'   				=> 'DESC',
			            'suppress_filters' 		=> true,
						'tax_query'            	=> array(
							array(
								'taxonomy' 	=> 'appcategory',
								'terms' 	=> $app->term_id
							)
						)
					);
					$query = new WP_Query( $args ); ?>
					<?php while ( $query->have_posts() ) : $query->the_post(); ?>
					<div class="infoSlide">
						<p><span class="infoslide-name">Version</span><span class="infoslide-value"><?php the_field( 'version' ); ?></span></p>
						<p><span class="infoslide-name">Uploaded</span><span class="infoslide-value"><?php if ( is_page_template( 'page-templates/homepage.php' ) ) : ?><?php echo esc_html( human_time_diff( get_the_time('U'), current_time('timestamp') ) ) . ' ago'; ?><?php else : ?><?php echo printf( __( '%s at %s', 'apk' ), get_the_date( 'F j, Y '), get_the_date( 'g:iA T' ) ); ?><?php endif; ?></span>
						</p>
						<p><span class="infoslide-name">File Size</span><span class="infoslide-value"><?php echo ms_get_file_size( get_the_ID() ); ?></span></p>
						<p><span class="infoslide-name">Downloads</span><span class="infoslide-value"><?php echo ms_get_download_count(get_the_ID() ); ?></span></p>
					</div>
					<?php endwhile; wp_reset_postdata(); ?>
				<?php endforeach; ?>
			<?php endif; ?>

		</div><!-- end .card -->
	</div><!-- end #recent-apps -->
</div><!-- end col -->

<?php get_sidebar(); ?>

<?php get_footer(); ?>