<?php
require_once( "../../../wp-load.php" );
$download_id = isset( $_GET['id'] ) && $_GET['id'] != '' ? (int) $_GET['id'] : false;
$release_id = get_post_meta( $download_id, 'release', true );
$file = get_field( 'apk_file', $download_id );
if ( $file ) {
	$key = '_download_count';
	$file = $file['url'];
	// download file count
	$file_count = get_post_meta( $download_id, $key, true );
	if ( $file_count == false || $file_count == '' || $file_count == '0' ) {
		$file_count = 0;
	}
	$file_count++;
	update_post_meta( $download_id, $key, $file_count );
	// release download count
	if ( isset( $release_id[0] ) ) {
		$r_count = get_post_meta( $release_id[0], $key, true );
		if ( $r_count == false || $r_count == '' || $r_count == '0' ) {
			$r_count = 0;
		}
		$r_count++;
		update_post_meta( $release_id[0], $key, $r_count );
	}
	header('Content-Type: application/vnd.android.package-archive');
	header("Content-disposition: attachment; filename=\"" . basename($file) . "\""); 
	readfile($file);
	exit;
} else {
	header("Location: ".home_url());
	exit;
}