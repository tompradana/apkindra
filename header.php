<!DOCTYPE html>
<html <?php language_attributes(); ?>>
  <head>
    <meta charset="<?php bloginfo( 'charset' ); ?>" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="profile" href="https://gmpg.org/xfn/11" />
    <?php wp_head(); ?>
  </head>

  <body <?php body_class(); ?>>

  	<?php if ( '' <> ($script = get_option( 'apk_indra_body_code' ) ) ) : ?>
  		<?php echo stripslashes( htmlspecialchars_decode( $scripts ) ); ?>
  	<?php endif; ?>

    <div id="page">
      <header id="masthead" class="site-header">
        
        <?php get_header( 'top' ); ?>

        <div id="site-header-content">
          <div class="container">
            <div class="d-block d-lg-none d-xl-none">
              <?php get_header( 'mobile' ); ?>
            </div>

            <div class="<?php if ( is_singular( array( 'app_download', 'app_release' ) ) || is_tax( 'appcategory' ) ) { ?>d-block<?php } else { ?>d-none d-lg-block<?php } ?>">
              <?php ms_site_title(); ?>
            </div>

            <div class="navigation-bar">
              <div class="row d-flex align-items-center">
                <div class="col-lg-<?php if ( 
                is_front_page() 
                || is_404() 
                || is_tax( 'devcategory' ) 
                || is_category()
                || is_page()
                || is_search()
                ) { ?>8<?php } else { ?>12<?php } ?>">
                  <?php
                    if ( (is_front_page()) || is_404() ) { 
                    	$menu_name = 'main';
						$menu_items = array();
						if ( ( $locations = get_nav_menu_locations() ) && isset( $locations[ $menu_name ] ) ) {
				    		$menu 		= wp_get_nav_menu_object( $locations[ $menu_name ] );
				    		$menu_items = wp_get_nav_menu_items( $menu->term_id );
				    		$menu_items = ms_get_menu_childrens( $menu_items );
				    	};
				    	if ( !empty( $menu_items ) ) : ?>
				    		<nav class="menu-main-menu-container">
				    			<ul id="menu-main-menu" class="nav navbar-nav d-none d-lg-block d-xl-block">
				    				<?php foreach( $menu_items as $item ) : 
				    					$d_class = !empty( $item['childs'] ) ? ' dropdown' : '';
				    				?>
				    					<li class="menu-item<?php echo $d_class; ?>">
				    						<a<?php if ( '' <> $d_class ) { ?> class="dropdown-toggle" data-toggle="dropdown"<?php } ?> href="<?php echo $item['item']->url; ?>"><?php echo $item['item']->title; ?></a>
				    						<?php if ( !empty( $item['childs'] ) ) : ?>
				    							<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
											    	<?php foreach( $item['childs'] as $child_item ) : ?>
											    		<a class="dropdown-item" href="<?php echo $child_item['item']->url; ?>">
											    			<?php echo $child_item['item']->title; ?>
											    		</a>
											    	<?php endforeach; ?>
											  </div>
				    						<?php endif; ?>
				    					</li>
				    				<?php endforeach; ?>
				    			</ul>
				    		</nav>
				    	<?php endif; ?>
                    <?php } else {
                      ms_breadcrumb();
                    }
                  ?>
                </div>
                
                <?php if ( 
                  is_front_page() 
                  || is_tax( 'devcategory' ) 
                  || is_category()
                  || is_page()
                  || is_404()
                  || is_search()
                ) { ?>
                  <?php get_search_form(); ?>
                <?php } ?>

              </div>
            </div>
            <!-- end .navigation-bar -->
          </div>
        </div>
      </header><!-- #masthead -->

      <div id="main">
        <div class="container">
          <div class="row">
            
        

 