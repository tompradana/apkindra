		  </div><!-- end row -->
		</div><!-- end contaienr -->
	</div><!-- end main -->
	<footer class="site-footer pt-3 pb-3">
		<div class="container">
			<div class="footer-menu text-center">
				<?php
					$menu = wp_nav_menu( array(
						'theme_location' => 'footer',
						'echo' => false
					) ); 
					if ( $menu ) echo $menu;
				?>
			</div>
			<div class="rights text-center">
				<span><?php echo date('Y'); ?> &copy; <?php bloginfo('name'); ?>. <?php _e( 'All Rights Reserved.', 'apk' ); ?></span>
			</div>
		</div>
	</footer>
	<?php wp_footer(); ?>
</div><!-- end #page -->

<div id="mySidenav" class="sidenav">
	<div class="sidemenu-header d-flex align-items-center justify-content-between">
		<a href="<?php echo home_url( '/' ); ?>"><?php bloginfo( 'name' ); ?></a>
		<a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
	</div>
	<?php
	$menu = wp_nav_menu( array(
		'theme_location' => 'mobile',
		'menu_id' => 'mobile-menu-wrapper',
		'echo' => false
	) ); 
	if ( $menu ) echo $menu;
	?>
</div>

</body>
</html>