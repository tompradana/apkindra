(function($){
	console.log('init');
	// get app
	$('form[name="get_app"]').on('submit',function(e){
		e.preventDefault();
		var formData = $(this).serializeArray();
		formData.push({name:"action",value:"googleplay_get_app"});
		$.ajax({
			url: ms_.ajaxurl,
			type: 'POST',
			data: formData,
		}).done(function(e){
			if ( e.success === false ) {
				alert( e.data.message );
			} else {
				let form = $('[name="insert_app"]');

				form.find('[name="title"]').val(e.data.name + ' ' + e.data.appVersion);
				form.find('[name="version"]').val(e.data.appVersion);
				form.find('[name="whatsnew"]').val(e.data.recentChange);
				form.find('[name="categories"]').val(e.data.category.name);
				form.find('[name="developer"]').val(e.data.developerName);
				form.find('[name="appname"]').val(e.data.name);
				form.find('[name="icon"]').val(e.data.icon);
				form.find('[name="totalrating"]').val(e.data.numberVoters);
				form.find('[name="avgrating"]').val(parseFloat(e.data.score.toFixed(1)));
				form.find('[name="appuri"]').val(e.data.url);
				form.find('[name="description"]').val(e.data.description);

				form.find('[name="rawinfo"]').val(JSON.stringify(e.data, null, 2));

				console.log(e.data);
			}
		}).fail(function(xhr, ajaxOptions, thrownError) {
			alert( thrownError + " ["+xhr.status+"]");
		})
	});

	// insert app
	$('form[name="insert_app"]').on('submit', function(e){
		e.preventDefault();
		var formData = $(this).serializeArray();
		formData.push({name:"action",value:"googleplay_insert_app"});
		$.ajax({
			url: ms_.ajaxurl,
			type: 'POST',
			data: formData,
		}).done(function(e){
			if ( e.success === false ) {
				alert( e.data.message );
			} else {
				window.location.reload();
			}
		}).fail(function(xhr, ajaxOptions, thrownError) {
			alert( thrownError + " ["+xhr.status+"]");
		})
	})
})(jQuery);