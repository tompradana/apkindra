<?php
/**
 * Release manager
 */
define( 'MS_APK_RELEASE_MANAGER_VERSION', '1.0.0' );

function ms_release_manager_menu() {
	$title = __( "Release Manager", "apk" );
	$hook_suffix = add_submenu_page( 
		'edit.php?post_type=app_release', 
		$title, 
		$title, 
		'manage_options', 
		'release_manager', 
		'ms_release_manager'
	);
}
add_action( 'admin_menu', 'ms_release_manager_menu' );

function ms_release_manager() {
	include get_template_directory() . '/includes/release-manager/views/release-manager-html.php';
}

function ms_release_manager_css( $hook_suffix ) {
	if ( $hook_suffix === 'app_release_page_release_manager' ) {
		wp_enqueue_style( 'ms-m-style', get_theme_file_uri( 'includes/release-manager/assets/style.css' ), array(), time() );
		wp_enqueue_script( 'ms-m-scripts', get_theme_file_uri( 'includes/release-manager/assets/scripts.js' ), array( 'jquery' ), time(), true );
		wp_localize_script( 'ms-m-scripts', 'ms_', array(
			'ajaxurl' => admin_url( 'admin-ajax.php' ),
		) );
	}
}
add_action( 'admin_enqueue_scripts', 'ms_release_manager_css' );

function ms_release_manager_get_app() {
	$gplay 		= new \Nelexa\GPlay\GPlayApps();
	$response 	= "";
	try {
		$response = $gplay->getAppInfo($_REQUEST['appid']);
	} catch(Exception $e) {
		$response = $e->getMessage();
	}
	
	if ( !is_object( $response ) ) {
		error_log( $response );
		wp_send_json_error( [ 'message' => __( 'Terjadi Kesalahan, periksa kemabali ID APK.', 'apk' ) ] );
	}

	// otherwise success
	wp_send_json_success( $response );
}
add_action( 'wp_ajax_googleplay_get_app', 'ms_release_manager_get_app' );
add_action( 'wp_ajax_nopriv_googleplay_get_app', 'ms_release_manager_get_app' );

function ms_release_manager_insert_app() {
	$response = [];

	$response = $_REQUEST;

	// 1. insert categories
	$cat_ids = [];
	if ( isset( $_REQUEST['categories'] ) && !empty( $_REQUEST['categories'] ) ) {
		$new_cats = array_map( 'esc_attr', explode( ',', $_REQUEST['categories'] ) );
		foreach( $new_cats as $cat ) {
			$create_cat = wp_create_term( trim( $cat ), 'category' );
			if ( !is_wp_error( $create_cat ) ) {
				if ( is_array( $create_cat ) ) {
					$cat_ids[] = (int) $create_cat['term_id'];
				} else {
					$cat_ids[] = (int) $create_cat;
				}
			}
		}
	}

	// 2. insert developers
	$dev_ids = [];
	if ( isset( $_REQUEST['developer'] ) && !empty( $_REQUEST['developer'] ) ) {
		$new_dev = array_map( 'esc_attr', explode( ',', $_REQUEST['developer'] ) );
		foreach( $new_dev as $dev ) {
			$create_dev = wp_create_term( trim( $dev ), 'devcategory' );
			if ( !is_wp_error( $create_dev ) ) {
				if ( is_array( $create_dev ) ) {
					$dev_ids[] = (int) $create_dev['term_id'];
				} else {
					$dev_ids[] = (int) $create_dev;
				}
			}
		}
	}

	// 3. insert applications
	$app_ids = [];
	if ( isset( $_REQUEST['appname'] ) && !empty( $_REQUEST['appname'] ) ) {
		$new_app = array_map( 'esc_attr', explode( ',', $_REQUEST['appname'] ) );
		foreach( $new_app as $app ) {
			$create_app = wp_create_term( trim( $app ), 'appcategory' );
			if ( !is_wp_error( $create_app ) ) {
				if ( is_array( $create_app ) ) {
					$app_ids[] = (int) $create_app['term_id'];
				} else {
					$app_ids[] = (int) $create_app;
				}
			}
		}
	}

	// 3.1 Update application metadata
	if ( !empty( $app_ids ) ) {
		// update description
		if ( isset( $_REQUEST['description'] ) && !empty( $_REQUEST['description'] ) ) {
			wp_update_term( $app_ids[0], 'appcategory', [
				'description' => $_REQUEST['description']
			] );
		}

		// update avg rating
		if ( isset( $_POST['avgrating'] ) && !empty( $_POST['avgrating'] ) ) {
			update_term_meta( $app_ids[0], 'aggregate_rating', floatval( $_POST['avgrating'] ) );
		}

		// update total rating
		if ( isset( $_POST['totalrating'] ) && !empty( $_POST['totalrating'] ) ) {
			update_term_meta( $app_ids[0], 'total_rating', (int) $_POST['totalrating'] );
		}

		// link developer & app in vice versa
		if ( !empty( $dev_ids ) ) {
			update_term_meta( $app_ids[0], 'devcategory_id', $dev_ids[0] );
			update_term_meta( $dev_ids[0], 'appcategory_id', $app_ids[0] );
		}
		
		// link categories & app
		if ( !empty( $cat_ids ) ) {
			update_term_meta( $app_ids[0], 'appcat_id', $cat_ids[0] );
			// update_term_meta( $dev_id, 'appcat_id', $term_id );
		}

		// url
		if ( isset( $_POST['appuri'] ) && !empty( $_POST['appuri'] ) ) {
			update_term_meta( $app_ids[0], 'play_url', esc_url( $_POST['appuri'] ) );
		}
	}
	
	// 4. insert release
	$release_args =  [
		'post_type' 	=> 'app_release',
		'post_title'	=> $_REQUEST['title'],
		'post_content'	=> $_REQUEST['whatsnew'],
		'post_status'	=> 'publish',
		'meta_input'	=> [
			'rawinfo'	=> $_REQUEST['rawinfo']
		]
	];

	if ( isset( $_REQUEST['version'] ) && !empty( $_REQUEST['version'] ) ) {
		$release_args['meta_input']['version'] = esc_attr( $_REQUEST['version'] );
	}

	if ( !empty( $cat_ids ) ) {
		$release_args['tax_input']['category'] = implode( ',', $cat_ids );
	}

	if ( !empty( $dev_ids ) ) {
		$release_args['tax_input']['devcategory'] = implode( ',', $dev_ids );
	}

	if ( !empty( $app_ids ) ) {
		$release_args['tax_input']['appcategory'] = implode( ',', $app_ids );
	}

	$release_id = wp_insert_post( $release_args, true );

	if ( is_wp_error( $release_id ) ) {
		error_log( $release_id->get_error_message() );
		wp_send_json_error( [ 'message' => $release_id->get_error_message() ] );
	}

	wp_send_json_success( $release_id );
}
add_action( 'wp_ajax_googleplay_insert_app', 'ms_release_manager_insert_app' );
add_action( 'wp_ajax_nopriv_googleplay_insert_app', 'ms_release_manager_insert_app' );