<div id="ms-release-manager" class="ms-m-page wrap">
	<h1><?php _e( 'APK Release Manager', 'apk' ); ?></h1>
	<div class="container">
		<div class="inner">
			<form name="get_app">
				<div class="form-row">
					<label><?php _e( 'Application ID' ); ?><br/><small><?php _e( 'Eg: com.mojang.minecraftpe' ); ?></small></label>
					<input type="text" name="appid" required="true">
					<button class="button button-primary" type="submit"><?php _e( 'Get Info' ); ?></button>	
				</div>
			</form>
			<hr/>
			<form name="insert_app">
				<div class="form-row"></div>
				
				<h3>Release Info</h3>

				<div class="form-row">
					<label><?php _e( 'Release Title', 'apk' ); ?></label>
					<input type="text" id="title" name="title" required="true">
				</div>

				<div class="form-row">
					<label><?php _e( 'Version', 'apk' ); ?></label>
					<input type="text" id="version" name="version" required="true">
				</div>

				<div class="form-row textarea">
					<label><?php _e( 'What\'s New', 'apk' ); ?></label>
					<textarea id="whatsnew" name="whatsnew" cols="60" rows="8"></textarea>
				</div>

				<div class="form-row">
					<label><?php _e( 'Categories', 'apk' ); ?></label>
					<input type="text" id="categories" name="categories" required="true">
				</div>

				<h3>Developer Info</h3>

				<div class="form-row">
					<label><?php _e( 'Developer', 'apk' ); ?></label>
					<input type="text" id="developer" name="developer" required="true">
				</div>

				<h3>Application Info</h3>

				<div class="form-row">
					<label><?php _e( 'Application', 'apk' ); ?></label>
					<input type="text" id="appname" name="appname" required="true">
				</div>

				<div class="form-row">
					<label><?php _e( 'Icon', 'apk' ); ?></label>
					<input type="text" id="icon" name="icon" required="true">
				</div>

				<div class="form-row">
					<label><?php _e( 'Total Rating', 'apk' ); ?></label>
					<input type="text" id="totalrating" name="totalrating" required="true">
				</div>

				<div class="form-row">
					<label><?php _e( 'Aggregate Rating', 'apk' ); ?></label>
					<input type="text" id="avgrating" name="avgrating" required="true">
				</div>

				<div class="form-row">
					<label><?php _e( 'Google Play URL', 'apk' ); ?></label>
					<input type="text" id="appuri" name="appuri" required="true">
				</div>
				
				<div class="form-row textarea">
					<label><?php _e( 'Descriptions', 'apk' ); ?></label>
					<textarea id="description" name="description" cols="60" rows="8" required="true"></textarea>
				</div>

				<h3>RAW Info</h3>
				<div class="form-row textarea">
					<label><?php _e( 'Raw Data', 'apk' ); ?></label>
					<textarea id="rawinfo" name="rawinfo" cols="60" rows="8" required="true" readonly="true"></textarea>
				</div>

				<?php submit_button( __( 'Submit Release', 'apk' ), 'primary', 'post_release' ); ?>
			</form>
		</div>
	</div>
</div>
