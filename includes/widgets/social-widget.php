<?php
/**
 * Class MS_Social_Profile_Widget
 */
class MS_Social_Profile_Widget extends WP_Widget {

  /**
   * Constructs the new widget.
   *
   * @see WP_Widget::__construct()
   */
  public function __construct() {
	// Instantiate the parent object.
  	parent::__construct( 
  		'ms_social_network_apk', 
  		__( 'APK - Social Network', 'apk' ),
  		array(
  			'classname' => 'ms-soscial-network'
  		)
  	);
  }

  /**
   * The widget's HTML output.
   *
   * @see WP_Widget::widget()
   *
   * @param array $args     Display arguments including before_title, after_title,
   *                        before_widget, and after_widget.
   * @param array $instance The settings for the particular instance of the widget.
   */
  public function widget( $args, $instance ) {
  	$title = isset( $instance['title'] ) ? $instance['title'] : __( 'Follow', 'apk' );
  	$social = $instance['social'];
  	echo $args['before_widget'];
  	echo $args['before_title'] . $title . $args['after_title']; ?>

  	<?php if ( !empty( $social ) ) : ?>
  		<ul>
	  		<?php foreach( $social as $key => $val ) : ?>
	  			<?php if ( '' != $val ) : ?>
	  				<li>
	  					<a href="<?php echo $val; ?>" class="soc-<?php echo $key; ?>">
			  				<?php if ( $key == 'facebook_url' ) : ?>
			  					<i class="fab fa-facebook-f"></i>
			  				<?php elseif ( $key == 'twitter_url' ) : ?>
			  					<i class="fab fa-twitter"></i>
			  				<?php elseif ( $key == 'telegram_url' ) : ?>
			  					<i class="fab fa-telegram"></i>
			  				<?php else : ?>
			  					<i class="fas fa-rss"></i>
			  				<?php endif; ?>
			  			</a>
	  				</li>
	  			<?php endif; ?>
	  		<?php endforeach; ?>
	  	</ul>
  	<?php endif; ?>

  	<?php echo $args['after_widget'];
  }

  /**
   * The widget update handler.
   *
   * @see WP_Widget::update()
   *
   * @param array $new_instance The new instance of the widget.
   * @param array $old_instance The old instance of the widget.
   * @return array The updated instance of the widget.
   */
  public function update( $new_instance, $old_instance ) {
  	$instance          = $old_instance;
  	$instance['title'] = sanitize_text_field( $new_instance['title'] );
  	$instance['social'] = !empty( $new_instance['social'] ) ? array_map( 'esc_url', $new_instance['social'] ) : array();

  	return $instance;
  }

  /**
   * Output the admin widget options form HTML.
   *
   * @param array $instance The current widget settings.
   * @return string The HTML markup for the form.
   */
  public function form( $instance ) {
	// $field $this->get_field_id( key );
	// $field $this->get_field_name( key );
  	$instance = wp_parse_args(
  		(array) $instance,
  		array(
  			'title'     => __( 'Follow', 'apk' ),
  			'numposts'  => 10,
  			'time'      => '1'
  		)
  	);
  	?>
  	<p>
  		<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:', 'apk' ); ?></label>
  		<input id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" class="widefat" value="<?php echo $instance['title']; ?>">
  	</p>
  	<?php foreach( array( 'facebook_url', 'twitter_url', 'telegram_url', 'rss_url' ) as $name ) : ?>
  	<p>
  		<label><?php echo $name; ?></label>
  		<input id="<?php echo $this->get_field_id( 'social'); ?>" name="<?php echo $this->get_field_name( 'social' ); ?>[<?php echo $name; ?>]" type="text" class="widefat" value="<?php echo $instance['social'][$name]; ?>">
  	</p>
  	<?php endforeach; ?>
  	<?php
  }
}