<?php
/**
 * Class MS_Popular_APK_Widget
 */
class MS_Popular_APK_Widget extends WP_Widget {
 
  /**
   * Constructs the new widget.
   *
   * @see WP_Widget::__construct()
   */
  public function __construct() {
    // Instantiate the parent object.
    parent::__construct( 
      'ms_popular_apk', 
      __( 'APK - Popular Apps', 'apk' ),
      array(
        'classname' => 'popular-upload-apk'
      )
    );
  }
 
  /**
   * The widget's HTML output.
   *
   * @see WP_Widget::widget()
   *
   * @param array $args     Display arguments including before_title, after_title,
   *                        before_widget, and after_widget.
   * @param array $instance The settings for the particular instance of the widget.
   */
  public function widget( $args, $instance ) {
    $title = isset( $instance['title'] ) ? $instance['title'] : __( 'Latest Uploads', 'apk' );
    $numposts = isset( $instance['numposts'] ) ? $instance['numposts'] : 10;
    echo $args['before_widget'];
    echo $args['before_title'] . $title . $args['after_title']; ?>

    <?php
        $aargs  = array(
          'post_type'       => array( 'app_release' ),
          'posts_per_page'  => $numposts,
          'meta_key'        => '_download_count',
          'orderby'         => 'meta_value_num',
          'order'           => 'DESC',
          'meta_query' => array(
            array(
              'key'     => '_download_count',
              'value'   => 0,
              'compare' => '>',
              'type'    => 'numeric'
            )
          )
        );
        if ( $instance['time'] == '1' ) {
          $aargs['date_query'] = array(
            array(
              'after' => '-30 days',
              'column' => 'post_date'
            ),
          );
        }
        if ( $instance['time'] == '2' ) {
          $aargs['date_query'] = array(
            array(
              'after' => '-7 days',
              'column' => 'post_date'
            ),
          );
        }
        if ( $instance['time'] == '3' ) {
          $aargs['date_query'] = array(
            array(
              'after' => '24 hours ago',
            ),
          );
        }
        $aquery = new WP_Query( $aargs );
        if ( $aquery->have_posts() ) : ?>
          <?php while ( $aquery->have_posts() ) : $aquery->the_post(); ?>
            <?php get_template_part( 'parts/content-widget', 'loop' ); ?>
          <?php endwhile; ?>
      <?php endif; wp_reset_postdata(); wp_reset_postdata(); wp_reset_query(); ?>

    <?php echo $args['after_widget'];
  }
 
  /**
   * The widget update handler.
   *
   * @see WP_Widget::update()
   *
   * @param array $new_instance The new instance of the widget.
   * @param array $old_instance The old instance of the widget.
   * @return array The updated instance of the widget.
   */
  public function update( $new_instance, $old_instance ) {
    $instance          = $old_instance;
    $instance['title'] = sanitize_text_field( $new_instance['title'] );
    $instance['numposts'] = 0 !== abs( $new_instance['numposts'] ) ? abs( $new_instance['numposts'] ) : 10;
    $instance['time'] = $new_instance['time'];

    return $instance;
  }
 
  /**
   * Output the admin widget options form HTML.
   *
   * @param array $instance The current widget settings.
   * @return string The HTML markup for the form.
   */
  public function form( $instance ) {
    // $field $this->get_field_id( key );
    // $field $this->get_field_name( key );
    $instance = wp_parse_args(
      (array) $instance,
      array(
        'title'     => __( 'Popular Uploads', 'apk' ),
        'numposts'  => 10,
        'time'      => '1'
      )
    );
    ?>
    <p>
      <label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:', 'apk' ); ?></label>
      <input id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" class="widefat" value="<?php echo $instance['title']; ?>">
    </p>
    <p>
      <label for="<?php echo $this->get_field_id( 'numposts' ); ?>"><?php _e( 'Total Items:', 'apk' ); ?></label>
      <input id="<?php echo $this->get_field_id( 'numposts' ); ?>" name="<?php echo $this->get_field_name( 'numposts' ); ?>" type="number" class="widefat" value="<?php echo $instance['numposts']; ?>">
    </p>
    <p>
      <label for="<?php echo $this->get_field_id( 'time' ); ?>"><?php _e( 'Range:', 'apk' ); ?></label>
      <select id="<?php echo $this->get_field_id( 'time' ); ?>" name="<?php echo $this->get_field_name( 'time' ); ?>" class="widefat">
        <option value="1" <?php selected( $instance['time'], '1' ); ?>>30 days</option>
        <option value="2" <?php selected( $instance['time'], '2' ); ?>>7 days</option>
        <option value="3" <?php selected( $instance['time'], '3' ); ?>>24 hours</option>
      </select>
    </p>
    <?php
  }
}