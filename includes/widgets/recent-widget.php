<?php
/**
 * Class MS_Recent_APK_Widget
 */
class MS_Recent_APK_Widget extends WP_Widget {
 
  /**
   * Constructs the new widget.
   *
   * @see WP_Widget::__construct()
   */
  public function __construct() {
    // Instantiate the parent object.
    parent::__construct( 
      'ms_latest_upload_apk', 
      __( 'APK - Latest Uploads', 'apk' ),
      array(
        'classname' => 'latest-upload-apk'
      )
    );
  }
 
  /**
   * The widget's HTML output.
   *
   * @see WP_Widget::widget()
   *
   * @param array $args     Display arguments including before_title, after_title,
   *                        before_widget, and after_widget.
   * @param array $instance The settings for the particular instance of the widget.
   */
  public function widget( $args, $instance ) {
    $title = isset( $instance['title'] ) ? $instance['title'] : __( 'Latest Uploads', 'apk' );
    $numposts = isset( $instance['numposts'] ) ? $instance['numposts'] : 10;
    echo $args['before_widget'];
    echo $args['before_title'] . $title . $args['after_title']; ?>

    <?php
        $aargs  = array(
          'post_type' => array( 'app_release' ),
          'posts_per_page' => $numposts
        );
        $query = new WP_Query( $aargs );
        if ( $query->have_posts() ) : ?>
          <?php while ( $query->have_posts() ) : $query->the_post(); ?>
            <?php get_template_part( 'parts/content', 'loop' ); ?>
          <?php endwhile; ?>
      <?php endif; wp_reset_postdata(); ?>

      <div class="text-center pb-3 pt-3">
        <a href="#"><?php _e( 'See more uploads', 'apk' ); ?></a>
      </div>
    
    <?php echo $args['after_widget'];
  }
 
  /**
   * The widget update handler.
   *
   * @see WP_Widget::update()
   *
   * @param array $new_instance The new instance of the widget.
   * @param array $old_instance The old instance of the widget.
   * @return array The updated instance of the widget.
   */
  public function update( $new_instance, $old_instance ) {
    $instance          = $old_instance;
    $instance['title'] = sanitize_text_field( $new_instance['title'] );
    $instance['numposts'] = 0 !== abs( $new_instance['numposts'] ) ? abs( $new_instance['numposts'] ) : 10;

    return $instance;
  }
 
  /**
   * Output the admin widget options form HTML.
   *
   * @param array $instance The current widget settings.
   * @return string The HTML markup for the form.
   */
  public function form( $instance ) {
    // $field $this->get_field_id( key );
    // $field $this->get_field_name( key );
    $instance = wp_parse_args(
      (array) $instance,
      array(
        'title'     => __( 'Latest Uploads', 'apk' ),
        'numposts'  => 10
      )
    );
    ?>
    <p>
      <label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:', 'apk' ); ?></label>
      <input id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" class="widefat" value="<?php echo $instance['title']; ?>">
    </p>
    <p>
      <label for="<?php echo $this->get_field_id( 'numposts' ); ?>"><?php _e( 'Total Items:', 'apk' ); ?></label>
      <input id="<?php echo $this->get_field_id( 'numposts' ); ?>" name="<?php echo $this->get_field_name( 'numposts' ); ?>" type="number" class="widefat" value="<?php echo $instance['numposts']; ?>">
    </p>
    <?php
  }
}