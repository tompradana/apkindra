<?php
// register taxonomies
function ms_register_taxonomies() {
	register_taxonomy( 'devcategory', array( 'app_release'/*, 'app_download' */ ), array(
		'label'               => __( 'Developers', 'apk' ),
		'public'              => true,
		'has_archive'         => true,
		'hierarchical'        => true,
		'show_in_rest'        => true,
		'show_in_quick_edit'  => true,
		'show_admin_column'   => true,
		'labels'			  => [
			'add_new_item'	=> __( 'Add new Developer', 'apk' )
		]
		//'rewrite'             => array( 'slug' => '/', 'with_front' => false ),
	) );

	register_taxonomy( 'appcategory', array( 'app_release'/*, 'app_download' */), array(
		'label'               => __( 'Applications', 'apk' ),
		'public'              => true,
		'has_archive'         => true,
		'hierarchical'        => true,
		'show_in_rest'        => true,
		'show_in_quick_edit'  => true,
		'show_admin_column'   => true,
		'labels'			  => [
			'add_new_item'	=> __( 'Add new Application', 'apk' )
		]
		//'rewrite'             => array( 'slug' => '/', 'with_front' => false ),
	) );
}
add_action( 'init', 'ms_register_taxonomies', 99 );

// change term link
function ms_change_term_permalink( $url, $term, $taxonomy ){
	if ( in_array( $taxonomy, array( 'devcategory', 'appcategory' ) ) ) {
		if ( $taxonomy === 'appcategory' ) {
			$dev_id = get_term_meta( $term->term_id, 'devcategory_id', true );
			if ( $dev_id ) {
				$dev_id_obj = get_term_by( 'id', $dev_id, 'devcategory' );
				if ( isset( $dev_id_obj->slug ) ) {
					$url = str_replace('/'.$taxonomy, '/apk/'.$dev_id_obj->slug, $url);
				}
			}
		} else {
			$url = str_replace('/' . $taxonomy, '/apk', $url);  
		}
	}

	return $url;
}
add_filter( 'term_link', 'ms_change_term_permalink', 10, 3 );

// Add new relationship field
function ms_taxonomy_add_form_fields( $taxonomy ) {
	wp_enqueue_media();
	// this will add the custom meta field to the add new term page
	$devs = get_terms( array( 'taxonomy' => 'devcategory', 'hide_empty' => 0 ) );
	$cats = get_terms( array( 'taxonomy' => 'category', 'hide_empty' => 0 ) );
	?>
	<div class="form-field">
		<label for="series_image">
		<?php _e( 'Application Icon', 'apk' ); ?></label>
		<input type="text" name="app_icon_url" id="app_icon" class="app-icon-url">
		<input type="hidden" name="app_icon_id" id="app_icon_id" class="app-icon-id">
		<input class="upload_image_button button" id="_add_app_icon" type="button" value="Select App Icon" />
		<script>
		jQuery(document).ready(function($) {
			var mediaUploader;
			jQuery('#_add_app_icon').click(function(e) {
				e.preventDefault();
				// If the uploader object has already been created, reopen the dialog
				if (mediaUploader) {
					mediaUploader.open();
					return;
				}
				// Extend the wp.media object
				mediaUploader = wp.media.frames.file_frame = wp.media({
					title: 'Choose Image',
					button: {
						text: 'Choose Image'
					},
					multiple: false
				});

				// When a file is selected, grab the URL and set it as the text field's value
				mediaUploader.on('select', function() {
					var attachment = mediaUploader.state().get('selection').first().toJSON();
					jQuery('.app-icon-id').val(attachment.id)
					jQuery('#app-icon-preview').attr("src", attachment.url)
					jQuery('.app-icon-url').val(attachment.url)
				});
				// Open the uploader dialog
				mediaUploader.open();
			});
		});
		</script>
	</div>

	<div class="form-field">
		<label for="appcat_id"><?php _e( 'Categories', 'apk' ); ?></label>
		<select name="appcat_id" id="appcat_id" class="widefat" required="true">
			<option value="">- Select Category -</option>
			<?php foreach( $cats as $cat ) {
				printf( '<option value="%d">%s</option>', $cat->term_id, $cat->name ); 
			}; ?>
		</select>
		<p class="description"><?php _e( 'Enter a value for this field','apk' ); ?></p>
	</div>

	<div class="form-field">
		<label for="devcategory_id"><?php _e( 'Developer', 'apk' ); ?></label>
		<select name="devcategory_id" id="devcategory_id" class="widefat" required="true">
			<option value="">- Select Developer -</option>
			<?php foreach( $devs as $dev ) {
				printf( '<option value="%d">%s</option>', $dev->term_id, $dev->name ); 
			}; ?>
		</select>
		<p class="description"><?php _e( 'Enter a value for this field','apk' ); ?></p>
	</div>

	<div class="form-field">
		<label for="total_rating">
		<?php _e( 'Total Rating', 'apk' ); ?></label>
		<input type="number" min="0" name="total_rating">
	</div>

	<div class="form-field">
		<label for="aggregate_rating">
		<?php _e( 'Aggregate Rating 1-5', 'apk' ); ?></label>
		<input type="number" min="0" max="5" step="0.1" name="aggregate_rating">
	</div>

	<div class="form-field">
		<label for="play_url">
		<?php _e( 'Google Play URL', 'apk' ); ?></label>
		<input type="text" name="play_url">
	</div>
<?php
}
add_action( 'appcategory_add_form_fields', 'ms_taxonomy_add_form_fields', 10, 1 );

// Edit term page
function ms_taxonomy_edit_form_fields( $term, $taxonomy ) {
	wp_enqueue_media();
	$devs = get_terms( array( 'taxonomy' => 'devcategory', 'hide_empty' => 0 ) );
	$cats = get_terms( array( 'taxonomy' => 'category', 'hide_empty' => 0 ) );
	$get = get_term_meta( $term->term_id, 'devcategory_id', true );
	$get_cat = get_term_meta( $term->term_id, 'appcat_id', true );
	$total_rating = get_term_meta( $term->term_id, 'total_rating', true );
	$aggregate_rating = get_term_meta( $term->term_id, 'aggregate_rating', true );
	$play_url = get_term_meta( $term->term_id, 'play_url', true );
	?>
	<tr class="form-field">
		<th scope="row" valign="top"><label for="_add_app_icon">
				<?php _e( 'Series Image', 'apk' ); ?></label></th>
		<td>
			<?php
				$app_icon_url = get_term_meta( $term->term_id, 'app_icon_url', true );
				$app_icon_id = get_term_meta( $term->term_id, 'app_icon_id', true );
			?>
			<input type="text" name="app_icon_url" id="app_icon" class="app-icon-url" value="<?php echo $app_icon_url; ?>">
			<input type="hidden" name="app_icon_id" id="app_icon_id" class="app-icon-id" value="<?php echo $app_icon_id; ?>">
			<input class="upload_image_button button" name="_add_app_icon" id="_add_app_icon" type="button" value="Select App Icon" />
		</td>
	</tr>

	<tr class="form-field">
		<th scope="row" valign="top"></th>
		<td style="height: 150px;">
			<style>
			div.img-wrap img {
				max-width: 450px;
			}
			</style>
			<div class="img-wrap">
				<img src="<?php echo $app_icon_url; ?>" id="app-icon-preview">
			</div>
			<script>
			jQuery(document).ready(function($) {
				var mediaUploader;
				jQuery('#_add_app_icon').click(function(e) {
					e.preventDefault();
					// If the uploader object has already been created, reopen the dialog
					if (mediaUploader) {
						mediaUploader.open();
						return;
					}
					// Extend the wp.media object
					mediaUploader = wp.media.frames.file_frame = wp.media({
						title: 'Choose Image',
						button: {
							text: 'Choose Image'
						},
						multiple: false
					});

					// When a file is selected, grab the URL and set it as the text field's value
					mediaUploader.on('select', function() {
						var attachment = mediaUploader.state().get('selection').first().toJSON();
						jQuery('.app-icon-id').val(attachment.id)
						jQuery('#app-icon-preview').attr("src",attachment.url)
						jQuery('.app-icon-url').val(attachment.url)
					});
					// Open the uploader dialog
					mediaUploader.open();
				});
			});
			</script>
		</td>
	</tr>

	<tr class="form-field">
		<th scope="row" valign="top"><label for="appcat_id"><?php _e( 'Category', 'apk' ); ?></label></th>
		<td>
			<select name="appcat_id" id="appcat_id" class="widefat" required="true">
				<option value="">- Select Category -</option>
				<?php foreach( $cats as $cat ) {
					printf( '<option value="%d"%s>%s</option>', 
						$cat->term_id, 
						selected( $get_cat, $cat->term_id, false ),
						$cat->name 
					); 
				}; ?>
			</select>
			<p class="description"><?php _e( 'Enter a value for this field','apk' ); ?></p>
		</td>
	</tr>

	<tr class="form-field">
		<th scope="row" valign="top"><label for="devcategory_id"><?php _e( 'Developer', 'apk' ); ?></label></th>
		<td>
			<select name="devcategory_id" id="devcategory_id" class="widefat" required="true">
				<option value="">- Select Developer -</option>
				<?php foreach( $devs as $dev ) {
					printf( '<option value="%d"%s>%s</option>', 
						$dev->term_id, 
						selected( $get, $dev->term_id, false ),
						$dev->name 
					); 
				}; ?>
			</select>
			<p class="description"><?php _e( 'Enter a value for this field','apk' ); ?></p>
		</td>
	</tr>

	<tr class="form-field">
		<th scope="row" valign="top">
			<label><?php _e( 'Total Rating', 'apk' ); ?></label>
		</th>
		<td>
			<input type="number" min="0" name="total_rating" value="<?php echo $total_rating; ?>">
		</td>
	</tr>

	<tr class="form-field">
		<th scope="row" valign="top">
			<label><?php _e( 'Aggregate Rating 1-5', 'apk' ); ?></label>
		</th>
		<td>
			<input type="number" min="0" max="5" step="0.1" name="aggregate_rating" value="<?php echo $aggregate_rating; ?>">
		</td>
	</tr>

	<tr class="form-field">
		<th scope="row" valign="top">
			<label><?php _e( 'Google Play URL', 'apk' ); ?></label>
		</th>
		<td>
			<input type="text" name="play_url" value="<?php echo $play_url; ?>">
		</td>
	</tr>
<?php
}
add_action( 'appcategory_edit_form_fields', 'ms_taxonomy_edit_form_fields', 10, 2 );

// A callback function to save our extra taxonomy field(s)  
function save_taxonomy_custom_meta( $term_id, $term_taxonmy_id ) {
	if ( isset( $_POST['devcategory_id'] ) && !empty( $_POST['devcategory_id'] ) ) {
		$dev_id = $_POST['devcategory_id'];
		
		update_term_meta( $term_id, 'devcategory_id', $dev_id );
		update_term_meta( $dev_id, 'appcategory_id', $term_id );
	}

	if ( isset( $_POST['appcat_id'] ) && !empty( $_POST['appcat_id'] ) ) {
		$dev_id = $_POST['appcat_id'];
		
		update_term_meta( $term_id, 'appcat_id', $dev_id );
		// update_term_meta( $dev_id, 'appcat_id', $term_id );
	}

	if ( isset( $_POST['app_icon_url'] ) && !empty( $_POST['app_icon_url'] ) ) {
		$icon_url = $_POST['app_icon_url'];
		
		update_term_meta( $term_id, 'app_icon_url', $icon_url );
	}

	if ( isset( $_POST['app_icon_id'] ) && !empty( $_POST['app_icon_id'] ) ) {
		$icon_id = $_POST['app_icon_id'];
		
		update_term_meta( $term_id, 'app_icon_id', $icon_id );
	}

	if ( isset( $_POST['total_rating'] ) && !empty( $_POST['total_rating'] ) ) {
		$rating_total = $_POST['total_rating'];
		update_term_meta( $term_id, 'total_rating', $rating_total );
	}

	if ( isset( $_POST['aggregate_rating'] ) && !empty( $_POST['aggregate_rating'] ) ) {
		$rating_agg = $_POST['aggregate_rating'];
		
		update_term_meta( $term_id, 'aggregate_rating', $rating_agg );
	}

	if ( isset( $_POST['play_url'] ) && !empty( $_POST['play_url'] ) ) {
		$play_url = $_POST['play_url'];
		
		update_term_meta( $term_id, 'play_url', esc_url( $play_url ) );
	}
}  
add_action( 'edited_appcategory', 'save_taxonomy_custom_meta', 10, 2 );  
add_action( 'create_appcategory', 'save_taxonomy_custom_meta', 10, 2 );

// dev columns
function ms_devcategory_columns($ms_devcategory_columns) {
	$new_columns = array(
		'cb'            => '<input type="checkbox" />',
		'icon'          => '&nbsp;',
		'name'          => __('Name','apk'),
		'appdeveloper'  => __('Developer','apk'),
		'description'   => __( 'Description','apk' ),
		'slug'          => __('Slug','apk'),
		'posts'         => __('Releases','apk')
	);
	unset( $new_columns['description'] );
	return $new_columns;
}
add_filter( "manage_edit-appcategory_columns", 'ms_devcategory_columns' );

// Add to admin_init function    
function ms_manage_devcategory_columns( $out, $column_name, $category_id ) {
	switch ( $column_name ) {
		case 'icon':
			$icon_id = (int) get_term_meta( $category_id, 'app_icon_id', true );
			if ( $icon_id ) {
				echo wp_get_attachment_image( $icon_id, array(64,64), true );
			}
			break; 
		case 'appdeveloper': 
			$meta = get_term_meta( $category_id, 'devcategory_id', true );
			if ( $meta ) {
				$dev = get_term_by( 'id', $meta, 'devcategory' );
				$out .= $dev->name;
			} else {
				$out .= '-';
			}
			break;
		default:
			break;
		}
	return $out;    
}
add_filter( "manage_appcategory_custom_column", 'ms_manage_devcategory_columns', 10, 3);

// add_action('template_redirect', 'rudr_old_term_redirect');
// function rudr_old_term_redirect() {
//   $taxonomy_name = 'devcategory';
//   $taxonomy_slug = 'devcategory';
 
//   // exit the redirect function if taxonomy slug is not in URL
//   if( strpos( $_SERVER['REQUEST_URI'], $taxonomy_slug ) === FALSE)
//     return;
 
//   if( ( is_category() && $taxonomy_name=='category' ) || ( is_tag() && $taxonomy_name=='post_tag' ) || is_tax( $taxonomy_name ) ) :
 
//           wp_redirect( site_url( str_replace($taxonomy_slug, '', $_SERVER['REQUEST_URI']) ), 301 );
//     exit();
 
//   endif;
 
// }