<?php
function ms_register_widgets_area() {
  register_sidebar( array(
      'name'          => __( 'Sidebar', 'apk' ),
      'id'            => 'sidebar',
      'description'   => __( 'Widgets in this area will be shown on all posts and pages.', 'apk' ),
      'before_widget' => '<div id="%1$s" class="widget card mb-3 %2$s">',
      'after_widget'  => '</div>',
      'before_title'  => '<h5 class="widget-title">',
      'after_title'   => '</h5>',
  ) );

  register_sidebar( array(
    'name'          => __( 'Ads 728 Top', 'apk' ),
    'id'            => 'ads-72890',
    'description'   => __( 'Ads slot.', 'apk' ),
    'before_widget' => '<div id="%1$s" class="widget advtsm no-padding card mb-3 %2$s">',
    'after_widget'  => '</div>',
    'before_title'  => '<h5 class="widget-title">',
    'after_title'   => '</h5>',
  ) );

  register_sidebar( array(
    'name'          => __( 'Ads 728 Bottom', 'apk' ),
    'id'            => 'ads-72890-bottom',
    'description'   => __( 'Ads slot.', 'apk' ),
    'before_widget' => '<div id="%1$s" class="widget advtsm no-padding card mb-3 %2$s">',
    'after_widget'  => '</div>',
    'before_title'  => '<h5 class="widget-title">',
    'after_title'   => '</h5>',
  ) );

  register_sidebar( array(
    'name'          => __( 'Ads 728 Developer List', 'apk' ),
    'id'            => 'ads-dev-72890',
    'description'   => __( 'Ads slot.', 'apk' ),
    'before_widget' => '<div id="%1$s" class="widget advtsm no-padding card mb-3 %2$s">',
    'after_widget'  => '</div>',
    'before_title'  => '<h5 class="widget-title">',
    'after_title'   => '</h5>',
  ) );

  register_sidebar( array(
    'name'          => __( 'Ads 728 Release Detail 1', 'apk' ),
    'id'            => 'ads-release_1-72890',
    'description'   => __( 'Ads slot.', 'apk' ),
    'before_widget' => '<div id="%1$s" class="widget advtsm no-padding card mb-3 %2$s">',
    'after_widget'  => '</div>',
    'before_title'  => '<h5 class="widget-title">',
    'after_title'   => '</h5>',
  ) );

  register_sidebar( array(
    'name'          => __( 'Ads 728 Release Detail 2', 'apk' ),
    'id'            => 'ads-release_2-72890',
    'description'   => __( 'Ads slot.', 'apk' ),
    'before_widget' => '<div id="%1$s" class="widget advtsm no-padding card mb-3 %2$s">',
    'after_widget'  => '</div>',
    'before_title'  => '<h5 class="widget-title">',
    'after_title'   => '</h5>',
  ) );

  register_sidebar( array(
    'name'          => __( 'Ads 330 Download Detail 1', 'apk' ),
    'id'            => 'ads-download_1-330',
    'description'   => __( 'Ads slot.', 'apk' ),
    'before_widget' => '<div id="%1$s" class="widget advtsm no-padding card mb-3 %2$s">',
    'after_widget'  => '</div>',
    'before_title'  => '<h5 class="widget-title">',
    'after_title'   => '</h5>',
  ) );

  register_sidebar( array(
    'name'          => __( 'Ads 728 Download Detail 2', 'apk' ),
    'id'            => 'ads-download_2-72890',
    'description'   => __( 'Ads slot.', 'apk' ),
    'before_widget' => '<div id="%1$s" class="widget advtsm no-padding card mb-3 %2$s">',
    'after_widget'  => '</div>',
    'before_title'  => '<h5 class="widget-title">',
    'after_title'   => '</h5>',
  ) );

  register_sidebar( array(
    'name'          => __( 'Ads 728 Download Detail 3', 'apk' ),
    'id'            => 'ads-download_3-72890',
    'description'   => __( 'Ads slot.', 'apk' ),
    'before_widget' => '<div id="%1$s" class="widget advtsm no-padding card mb-3 %2$s">',
    'after_widget'  => '</div>',
    'before_title'  => '<h5 class="widget-title">',
    'after_title'   => '</h5>',
  ) );
}
add_action( 'widgets_init', 'ms_register_widgets_area' );

function ms_register_widgets() {
  register_widget( 'MS_Recent_APK_Widget' );
  register_widget( 'MS_Popular_APK_Widget' );
  register_widget( 'MS_Social_Profile_Widget' );
}
add_action( 'widgets_init', 'ms_register_widgets' );
