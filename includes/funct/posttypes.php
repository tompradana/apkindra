<?php
// register post types
function ms_register_posttypes() {

	add_rewrite_tag('%devcat%','(.+)');
	add_rewrite_tag('%appcat%','(.+)');

	$args = array(
		'label'               => __( 'APK Releases', 'apk' ),
		'public'              => true,
		'hierarchical'        => true,
		'has_archive'         => true,
		'show_ui'             => true,
		'show_in_nav_menus'   => false,
		'show_in_rest'        => true,
		'menu_position'       => 5,
		'taxonomies'          => array( 'post_tag', 'category' ),
		'supports'            => array( 'title', 'editor', 'comments', 'thumbnail', 'page-attributes' )
	);
	register_post_type( 'app_release', $args );

	$args = array(
		'label'               => __( 'APK Files', 'apk' ),
		'public'              => true,
		'hierarchical'        => true,
		'has_archive'         => false,
		'exclude_from_search' => true,
		'show_ui'             => true,
		'show_in_nav_menus'   => false,
		'show_in_rest'        => true,
		'menu_position'       => 5,
	// 'taxonomies'          => array( 'post_tag'/*, 'category' */),
		'supports'            => array( 'title', /*'editor',*/ 'comments', 'thumbnail', 'page-attributes' )
	);
	register_post_type( 'app_download', $args );
}
add_action( 'init', 'ms_register_posttypes', 0 );

// remove block editor
function ms_disable_gutenberg( $current_status, $post_type ) {
	// Use your post type key instead of 'product'
	if ( in_array($post_type, array('app_release','app_download'))) return false;
	return $current_status;
}
add_filter( 'use_block_editor_for_post_type', 'ms_disable_gutenberg', 10, 2 );

//
function wpa_show_permalinks( $post_link, $post, $leavename ){
	if ($post->post_type == 'app_release' ) {
		$add = array();

		if ( $cats = get_the_terms( $post->ID, 'devcategory') ) {
			$add[] = array_pop($cats)->slug;
		}

		if ( $cats = get_the_terms( $post->ID, 'appcategory') ) {
			$add[] = array_pop($cats)->slug;
		}

		if ( !empty( $add ) && count( $add ) == 2 ) {
			$add[] = $post->post_name . '-apk-release';
			return home_url('/apk/'.implode( '/', $add ));
		}

		return $post_link;
	}

	if ( $post->post_type === 'app_download' ) {
		$release = get_field( 'release', $post->ID );
		if ( $release ) {
			$release      = get_post( $release[0] );
			$release_url  = isset( $release->ID ) ? rtrim( get_permalink( $release->ID ), '/' ) : '';
			$post_link    = $release_url . '/' . $post->post_name . '/';
			$post_link    = preg_replace( '~\/(?!.*\/)~', '-apk-download/', $post_link );
		}

		return $post_link;
	}

	return $post_link;
}
add_filter( 'post_type_link', 'wpa_show_permalinks', 10, 3 );

// Fix request
function ms_change_query_request( $query ){
	if ( is_admin() ) {
		return $query;
	}
	global $wp;
	$shost 			= $_SERVER['HTTP_HOST'];
	$surl 			= $_SERVER['REQUEST_URI'];
	$actual_link 	= ( is_ssl() ? "https" : "http") . "://$shost$surl";
	$path 			= str_replace( home_url('/'), '', $actual_link );
	$path_count 	= count(explode('/', rtrim($path,'/') ) );


	if ( $path_count === 5 ) {
		preg_match('/^apk\/?([^\/]*)\/([^\/]*)\/([^\/]*)\/([^\/]*)\/?/', $path, $matches );
		if ( isset( $matches[4] ) && false !== strpos( $matches[4], '-apk-download' ) ) {
			$post_type = 'app_download';
			$post_name = str_replace( '-apk-download', '', rtrim( $matches[4], '/') );
			$post_objt = get_page_by_path( $post_name, OBJECT, $post_type );
			if ( $post_objt ) {
				$query['name'] = $post_objt->post_name;
				$query['post_type'] = $post_objt->post_type;
				if( isset( $query['error'] ) ) unset( $query['error'] );
			}
		}
	}

	if ( $path_count === 4 ) {
		preg_match('/^apk\/?([^\/]*)\/([^\/]*)\/([^\/]*)\/?/', $path, $matches );
		if ( isset( $matches[3] ) && false !== strpos( $matches[3], '-apk-release' ) ) {
			$post_type = 'app_release';
			$post_name = str_replace( '-apk-release', '', rtrim( $matches[3], '/') );
			$post_objt = get_page_by_path( $post_name, OBJECT, $post_type );
			if ( $post_objt ) {
				$query['name'] = $post_objt->post_name;
				$query['post_type'] = $post_objt->post_type;
				if( isset( $query['error'] ) ) unset( $query['error'] );
			}
		}
	}

	if ( $path_count === 3 ) {
		preg_match('/^apk\/?([^\/]*)\/([^\/]*)\/?/', $path, $matches );
		$name = isset( $matches[2] ) ? rtrim( $matches[2], '/') : '';
		$term = get_term_by( 'slug', $name, 'appcategory' );

		if ( $term !== false ) {
			$query['appcategory'] = $name;
			if( isset( $query['error'] ) ) unset( $query['error'] );
			if( isset( $query['attachment'] ) ) unset( $query['attachment'] );
		}
	}

	if ( $path_count === 2 ) {
		preg_match('/^apk\/?([^\/]*)\/?/', $path, $matches );
		$name = isset( $matches[1] ) ? rtrim( $matches[1], '/') : '';
		$term = get_term_by( 'slug', $name, 'devcategory' );

		if ( $term !== false ) {
			$query['devcategory'] = $name;
			if( isset( $query['error'] ) ) unset( $query['error'] );
			if( isset( $query['attachment'] ) ) unset( $query['attachment'] );
		}
	}

	return $query;
}
add_filter( 'request', 'ms_change_query_request', 1, 1 );

function ms_post_type_order( $wp_query ) {
	if (is_admin() && !isset( $_GET['orderby'] ) ) {
		$post_type = $wp_query->query['post_type'];
		if ( $post_type == 'app_download' || $post_type == 'app_release' ) {
			$wp_query->set('orderby', 'date');
			$wp_query->set('order', 'DESC');
		}
	}
}
add_filter('pre_get_posts', 'ms_post_type_order');

add_action( 'save_post', 'ms_resave_appdev_data', 10, 2 );
function ms_resave_appdev_data( $post_id, $post ) {
	if ( 'app_release' !== $post->post_type ) {
        return;
    }

    if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
		return;
	}

	$dev_id = 0;
	$app_id = 0;

	if ( $devs = get_the_terms( $post_id, 'devcategory' ) ) {
        $dev_id = array_pop($devs)->term_id;
    }

    if ( $apps = get_the_terms( $post_id, 'appcategory' ) ) {
        $app_id = array_pop($apps)->term_id;
    }

    // error_log( 'appid ' . $app_id );
    // error_log( 'devid ' . $dev_id );

 	if ( $dev_id !== 0 && $app_id !== 0 ) {
		update_term_meta( $app_id, 'devcategory_id', $dev_id );
	 	update_term_meta( $dev_id, 'appcategory_id', $app_id );
	}
}