<?php
// add submenu
function ms_settings() {
	add_options_page( 
        __( 'APK Theme Settings', 'textdomain' ),
        __( 'Theme Settings', 'textdomain' ),
        'manage_options',
        'theme-settings.php',
        'ms_settings_page'
    );
}
add_action( 'admin_menu', 'ms_settings' );

// settings configuration
function ms_settings_config() {
	$opt = apply_filters( 'ms_settings_config_opt', 'apk_indra' );
	$config = array(
		array(
			'id' 		=> $opt . '_header_code',
			'label' 	=> __( 'Header Code', 'apk' ),
			'type' 		=> 'textarea',
			'desc'		=> __( 'Insert code inside <head></head> tag', 'apk' ),
			'default'	=> ''
		),
		array(
			'id' 		=> $opt . '_body_code',
			'label' 	=> __( 'Body Code', 'apk' ),
			'type' 		=> 'textarea',
			'desc'		=> __( 'Insert code after <body> tag', 'apk' ),
			'default'	=> ''
		),
		array(
			'id' 		=> $opt . '_footer_code',
			'label' 	=> __( 'Footer Code', 'apk' ),
			'type' 		=> 'textarea',
			'desc'		=> __( 'Insert code before </body> tag', 'apk' ),
			'default'	=> ''
		)
	);
	return apply_filters( 'ms_settings_config', $config );
}

// save settings
function ms_save_settings() {
	$config = ms_settings_config();
	if ( isset( $_POST['save_theme_settings'] ) ) :
		foreach( $config as $option ) :
			$id = $option['id'];
			if ( isset( $_POST[$id] ) ) {
				$val = htmlspecialchars( $_POST[$id] );
				update_option( $id, $val );
			}
		endforeach;
	endif;
}
add_action( 'load-settings_page_theme-settings', 'ms_save_settings' );

// settings page
function ms_settings_page() {
	$config = ms_settings_config();
	?>
	<div class="wrap">
		<h2>Theme Settings</h2>
		<form method="POST">
			<table class="form-table" role="presentation">
				<tbody>
					<?php foreach( $config as $field ) : 
						$label = isset( $field['label'] ) && !empty( $field['label'] ) ? esc_attr( $field['label'] ) : __( 'Label', 'apk' );
						$desc = isset( $field['desc'] ) && !empty( $field['desc'] ) ? esc_attr( $field['desc'] ) : '';
						$type = isset( $field['type'] ) && !empty( $field['type'] ) ? esc_attr( $field['type'] ) : 'text';
						$value = isset( $field['default'] ) && !empty( $field['default'] ) ? $field['default'] : '' ;
						if ( false !== get_option( $field['id'] ) ) {
							$value = stripslashes( get_option( $field['id'] ) );
						}
					?>
						<tr>
							<th scope="row">
								<?php echo $label; ?>
								<?php if ( '' <> $desc ) : ?>
									<br/><small><?php echo $desc; ?></small>
								<?php endif; ?>
							</th>
							<td>
								<?php if ( in_array( $type, array( 'text', 'number', 'email' ) ) ) : ?>

									<input type="<?php echo $type; ?>" name="<?php echo $field['id']; ?>" value="<?php echo $value; ?>">

								<?php elseif ( $type == 'textarea' ) : ?>

									<textarea class="widefat" rows="8" name="<?php echo $field['id']; ?>"><?php echo $value; ?></textarea>

								<?php else : ?>

									<input type="<?php echo $type; ?>" name="<?php echo $field['id']; ?>" value="<?php echo $value; ?>">

								<?php endif; ?>
							</td>
						</tr>
					<?php endforeach; ?>
				</tbody>
			</table>
			<?php submit_button( 'Save Settings', 'primary', 'save_theme_settings' ); ?>
		</form>
	</div>
	<?php
}

