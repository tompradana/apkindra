<?php
/**
 * Color extactor
 */
use League\ColorExtractor\Color;
use League\ColorExtractor\ColorExtractor;
use League\ColorExtractor\Palette;

/**
 * Upload apk
 */
function ms_custom_mimes ( $existing_mimes=array() ) {
	// ' with mime type '<code>application/vnd.android.package-archive</code>'
	$existing_mimes['apk'] = '<code>application/vnd.android.package-archive</code>';
	return $existing_mimes;
}
add_filter( 'upload_mimes', 'ms_custom_mimes' );

function ms_get_primary_icon_color() {
	if ( is_tax( 'appcategory' ) || is_singular( array( 'app_release', 'app_download' ) ) ) {
		$attachment_id = false;
		if ( is_tax() ) {
			$obj = get_queried_object();
			$attachment_id = get_term_meta( $obj->term_id, 'app_icon_id', true );
		} else {
			global $post;
			$post_id = $post->ID;
			if ( is_singular( 'app_download' ) ) {
				$release_id = get_post_meta( $post_id, 'release', true );
				$post_id = $release_id[0];
			}
			$obj = get_the_terms( $post_id, 'appcategory' );
			if ( !empty( $obj ) ) {
				$attachment_id = get_term_meta( $obj[0]->term_id, 'app_icon_id', true );
			}
		}

		$fullsize_path 	= get_attached_file( $attachment_id );
		if ( !$fullsize_path ) {
			return $fullsize_path;
		}

		if ( wp_get_image_mime( $fullsize_path ) == "image/webp" ) {
			return false;
		}

		$palette 		= Palette::fromFilename($fullsize_path);
		$extractor 		= new ColorExtractor($palette);
		$colors 		= $extractor->extract( 1 );
		if ( !empty( $colors ) ) {
			return Color::fromIntToHex( $colors[0] );
		} else {
			return false;
		}
	} else {
		return false;
	}
}

function ms_apk_page_color_scheme() {
	$color = ms_get_primary_icon_color();
	if ( $color ) {
		$style = "
			#site-header-top { background-color: $color }
		body.single #site-header-content .breadcrumbs>a,
		body.single #site-header-content .breadcrumbs,
		body.tax-appcategory #site-header-content .breadcrumbs>a,
		body.tax-appcategory #site-header-content .breadcrumbs,
		.breadcrumbs .variant-selector button,
			#app-tabs .nav-tabs .nav-link.active,
			#app-tabs .nav-tabs .nav-link,
		.single .card a,
		.single .card-with-tabs a,
		.tax-appcategory .nav-tabs a,
		.tax-appcategory .nav-tabs a.active,
		.tax-appcategory a,
		.app-meta .app-category .post-categories a, 
		.specrow .specicon .material-icons { color: $color }
			#app-tabs .nav-tabs .nav-link.active,
		.tax-appcategory .nav-tabs .nav-link.active { border-color: $color }
			#app-tabs .btn.btn-raised.btn-primary {
		color: #fff;
		background-color: $color;
		border-color: $color;
	}
	";
	printf( '<style>%s</style>', $style );
}
}
add_action( 'wp_head', 'ms_apk_page_color_scheme', 999 );

function ms_breadcrumb() { 
	global $post;
	$obj = get_queried_object();
	?>
	<div class="breadcrumbs">

		<?php if ( is_tax( 'devcategory' ) ) { ?>
			<span class="active"><?php single_term_title(); ?></span>
		<?php } else if ( is_tax( 'appcategory' ) ) { 
			$meta = get_term_meta( $obj->term_id, 'devcategory_id', true );
			?>
			<?php if ( $meta ) { 
				$dev = get_term_by( 'id', $meta, 'devcategory' );
				?>
				<a class="withoutripple" href="<?php echo get_term_link( $dev ); ?>"><?php echo $dev->name; ?></a>
				<i class="material-icons">keyboard_arrow_right</i> <a class="withoutripple active" href="<?php echo get_term_link( $obj ); ?>"><span><?php single_term_title(); ?></span></a> 
			<?php } else { ?>
				<span class="active"><?php single_term_title(); ?></span>
			<?php } ?>
		<?php } else if ( is_singular( array( 'app_release', 'app_download' ) ) ) { 
			$dev_cat 	= get_the_terms( $post->ID, 'devcategory' );
			$app_cat 	= get_the_terms( $post->ID, 'appcategory' );
			$title 		= $post->post_title;
			$release  = '';
			$release_id = $post->ID;
			if ( is_singular( 'app_download' ) ) {
				$meta = get_post_meta( $post->ID, 'release', true );
				if ( !empty( $meta ) ) {
					$release_id = $meta[0];
					$dev_cat 	= get_the_terms( $release_id, 'devcategory' );
					$app_cat 	= get_the_terms( $release_id, 'appcategory' );
					$title = get_the_title( $meta[0] );
					$release = ms_get_apk_files_counts( $meta[0], false );
				}
			}
			$version 	= '';
			preg_match_all('/([^\d]+)+?(\d+(?:\.\d+)+)/m', $title, $matches, PREG_SET_ORDER, 0);
			if ( !empty( $matches ) ) {
				$title = isset( $matches[0][1] ) ? trim( $matches[0][1] ) : $title;
				$version = isset( $matches[0][2] ) ? trim( $matches[0][2] ) : '';
			}
			$version = get_post_meta( $release_id, 'version', true );
			?>
			<?php if ( $dev_cat ) { ?>
				<a class="withoutripple" href="<?php echo get_term_link( $dev_cat[0] ); ?>"><?php echo $dev_cat[0]->name; ?></a>
			<?php } ?>
			<?php if ( $app_cat ) { ?>
				<i class="material-icons">keyboard_arrow_right</i> <a class="withoutripple" href="<?php echo get_term_link( $app_cat[0] ); ?>"><?php echo $title ?></a> 
			<?php } else { ?>
				<i class="material-icons">keyboard_arrow_right</i> <span class="active"><?php echo $title; ?></span>
			<?php } ?>
			<?php if ( $version ) { ?>
				<i class="material-icons">keyboard_arrow_right</i> <a class="withoutripple" href="<?php echo get_permalink( $release_id ); ?>"><?php echo $version; ?></a>
			<?php } ?>

			<?php if ( is_singular( 'app_download' ) ) : ?>
				<i class="material-icons">keyboard_arrow_right</i>
				<div class="btn-group variant-selector">
					<button type="button" class="btn<?php if ( $release > 1 ) {?> dropdown-toggle<?php } ?>" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" >
						<?php echo $current_arch = ms_get_apk_meta( 'architecture' ); ?>
					</button>
					<?php if ( $release > 1 ) { ?>
						<div class="dropdown-menu">
							<?php
							$args  = array(
								'post_type'       => 'app_download',
								'posts_per_page'  => -1,
								'order'           => 'ASC',
								'meta_key'        => 'release',
								'meta_value'      => $release_id,
								'meta_compare'    => 'LIKE'
							);
							$query = new WP_Query( $args );
							if ( $query->have_posts() ) : ?>
								<?php while ( $query->have_posts() ) : $query->the_post(); ?>
									<a class="dropdown-item<?php if ( $current_arch == ms_get_apk_meta( 'architecture' ) ) { ?> active<?php } ?>" href="<?php the_permalink(); ?>"><?php echo ms_get_apk_meta( 'architecture' ); ?></a>
								<?php endwhile; ?>
							<?php endif; ?>
							<?php wp_reset_postdata(); ?>
						</div>
					<?php } ?>
				</div>
			<?php endif ?>
		<?php } else if ( is_category() ) { 
			$obj = get_queried_object();
			?>
			<a class="withoutripple" href="<?php echo home_url('/categories/'); ?>"><?php _e( 'Categories', 'apk' );?></a><i class="material-icons">keyboard_arrow_right</i><span class="withoutripple active"><?php single_term_title(); ?></span>
		<?php } else { ?>
			<a class="withoutripple" href="<?php echo home_url('/'); ?>"><?php _e( 'Home', 'apk' );?></a> 
		<?php } ?>

		<?php if ( is_page() ) { ?>
			<i class="material-icons">keyboard_arrow_right</i> <span class="active"><?php echo get_the_title( $post->ID ); ?></span>
		<?php } ?>

		<?php if ( is_search() ) { ?>
			<i class="material-icons">keyboard_arrow_right</i> <span class="active"><?php _e( 'Search', 'apk' ); ?></span>
		<?php } ?>
	</div> 
<?php }

if ( !function_exists( 'make_clickable' ) ) {
	function _make_url_clickable_cb($matches) {
		$ret = '';
		$url = $matches[2];

		if ( empty($url) )
			return $matches[0];
		// removed trailing [.,;:] from URL
		if ( in_array(substr($url, -1), array('.', ',', ';', ':')) === true ) {
			$ret = substr($url, -1);
			$url = substr($url, 0, strlen($url)-1);
		}
		return $matches[1] . "<a href=\"$url\" rel=\"nofollow\">$url</a>" . $ret;
	}

	function _make_web_ftp_clickable_cb($matches) {
		$ret = '';
		$dest = $matches[2];
		$dest = 'http://' . $dest;

		if ( empty($dest) )
			return $matches[0];
		// removed trailing [,;:] from URL
		if ( in_array(substr($dest, -1), array('.', ',', ';', ':')) === true ) {
			$ret = substr($dest, -1);
			$dest = substr($dest, 0, strlen($dest)-1);
		}
		return $matches[1] . "<a href=\"$dest\" rel=\"nofollow\">$dest</a>" . $ret;
	}

	function _make_email_clickable_cb($matches) {
		$email = $matches[2] . '@' . $matches[3];
		return $matches[1] . "<a href=\"mailto:$email\">$email</a>";
	}

	function make_clickable($ret) {
		$ret = ' ' . $ret;
		// in testing, using arrays here was found to be faster
		$ret = preg_replace_callback('#([\s>])([\w]+?://[\w\\x80-\\xff\#$%&~/.\-;:=,?@\[\]+]*)#is', '_make_url_clickable_cb', $ret);
		$ret = preg_replace_callback('#([\s>])((www|ftp)\.[\w\\x80-\\xff\#$%&~/.\-;:=,?@\[\]+]*)#is', '_make_web_ftp_clickable_cb', $ret);
		$ret = preg_replace_callback('#([\s>])([.0-9a-z_+-]+)@(([0-9a-z-]+\.)+[0-9a-z]{2,})#i', '_make_email_clickable_cb', $ret);

		// this one is not in an array because we need it to run last, for cleanup of accidental links within links
		$ret = preg_replace("#(<a( [^>]+?>|>))<a [^>]+?>([^>]+?)</a></a>#i", "$1$3</a>", $ret);
		$ret = trim($ret);
		return $ret;
	}
}

function ms_rating( $app_id ) {
	$rate = (float) get_term_meta( $app_id, 'aggregate_rating', true );
	if ( $rate ) {
		?>
		<div class="star-rating">
			<?php for( $i = 1; $i <= 5; $i++ ) { ?>
				<?php if ( $rate >= $i) { 
					$star = '';
				} else if ( $rate < $i && $rate > ($i-1) ) {
					$star = '_half';
				} else {
					$star = '_border';
				}; ?>
				<i class="material-icons">star<?php echo $star; ?></i>
			<?php } ?>
		</div>
		<?php
	}
}

function ms_site_title() { 
	$icon_img = '';
	?>
	<div class="title-bar">
		<div class="site-title tag-title">
			<?php $a = false; if ( is_home() || is_front_page() || is_tax( 'devcategory' ) || is_page() || is_404() || is_search() ) { ?><a href="<?php echo home_url('/'); ?>"><?php $a = true; } else { ?><div class="d-flex align-items-center"><?php } ?>
			<?php 
			$title 	= get_the_title(); 
			$tag 		= get_bloginfo( 'description' );
			?>
			<?php if ( is_search() || is_home() || is_front_page() || is_tax( 'devcategory' ) || is_category() || is_page() || is_404() ) { ?>
				<?php $title = get_bloginfo( 'name' ); ?>
			<?php } else { 
				?>
				<?php if ( is_tax() ) {
					$obj 		= get_queried_object();
					$title 	= $obj->name;
					if ( '' <> ( $meta = get_term_meta( $obj->term_id, 'devcategory_id', true ) ) ) {
						$dev = get_term_by( 'id', $meta, 'devcategory' );
						$tag = sprintf( '%1$s <a href="%1$s">%2$s</a>', __( 'By', 'apk' ), $dev->name );
					}
					if ( '' <> ( $app_icon = get_term_meta( $obj->term_id, 'app_icon_id', true ) ) ) {
						$icon_img = wp_get_attachment_image( $app_icon, array(96,96), true );
					}
				} else if ( is_singular( array( 'app_release', 'app_download' ) ) ) {
					$post = get_queried_object();
					$obj = get_the_terms( $post->ID, 'appcategory' );
					if ( isset( $obj[0]->term_id ) && '' <> ( $app_icon = get_term_meta( $obj[0]->term_id, 'app_icon_id', true ) ) ) {
						$icon_img = wp_get_attachment_image( $app_icon, array(96,96), true );
					}
					if ( isset( $obj[0]->term_id ) && '' <> ( $meta = get_term_meta( $obj[0]->term_id, 'devcategory_id', true ) ) ) {
						$dev = get_term_by( 'id', $meta, 'devcategory' );
						$tag = sprintf( '%1$s <a href="%1$s">%2$s</a>', __( 'By', 'apk' ), $dev->name );
					}
					if ( is_singular( 'app_download' ) ) {
						global $post;
						$meta = get_post_meta( $post->ID, 'release', true );
						if ( !empty( $meta ) ) {
							$obj = get_the_terms( $meta[0], 'appcategory' );
							if ( '' <> ( $anu = get_term_meta( $obj[0]->term_id, 'devcategory_id', true ) ) ) {
								$dev = get_term_by( 'id', $anu, 'devcategory' );
								$tag = sprintf( '%1$s <a href="%1$s">%2$s</a>', __( 'By', 'apk' ), $dev->name );
							}
							if ( '' <> ( $app_icon = get_term_meta( $obj[0]->term_id, 'app_icon_id', true ) ) ) {
								$icon_img = wp_get_attachment_image( $app_icon, array(96,96), true );
							}
							$title = get_the_title( $meta[0] );
							$title .= sprintf( " (%s)", ms_get_apk_meta( 'architecture' ) );
						}
					}
				}; ?>
			<?php } ?>
			<?php if ( $icon_img ) {
				printf( '<div class="app-icon">%s</div>', $icon_img );
			}; ?>
			<div>
				<h1>
					<?php echo $title; ?>
				</h1>
				<p>
					<?php echo $tag; ?>
				</p>
			</div>
			<?php if ( $a ) { ?></a><?php } else { ?></div><?php } ?>
		</div>
	</div>
<?php }

function ms_get_apk_files_counts( $release_id, $display = true ) {
	if ( !$release_id ) return;
	$files = new WP_Query( array(
		'post_type' 			=> 'app_download',
		'posts_per_page'	=> -1,
		'meta_key'				=> 'release',
		'meta_value'			=> $release_id,
		'meta_compare'		=> 'LIKE'
	) );
	if ( $files->have_posts() ) {
		if ( $files->found_posts > 1 ) {
			if ( $display ) {
				echo sprintf( '<div class="vcount"><a class="variants-count" href="%s"><i class="material-icons">label</i>%s %s</a></div>', get_permalink( $release_id ), $files->found_posts, __( 'Variants', 'apk' ) );
			} else {
				return $files->found_posts;
			}
		} else {
			if ( !$display ) {
				return $files->found_posts;
			}
		}
	} else {
		return false;
	}
}

function ms_convert_to_byte( $from ) {
	$units = ['B', 'KB', 'MB', 'GB', 'TB', 'PB'];
	$number = substr($from, 0, -2);
	$suffix = strtoupper(substr($from,-2));

	//B or no suffix
	if(is_numeric(substr($suffix, 0, 1))) {
		return preg_replace('/[^\d]/', '', $from);
	}

	$exponent = array_flip($units)[$suffix] ?? null;
	if($exponent === null) {
		return null;
	}

	return $number * (1024 ** $exponent);
}

function ms_get_apk_meta( $meta = '', $id = 0 ) {
	global $post;
	if ( !function_exists( 'get_field' ) ) {
		return;
	}
	if ( $id != 0 ) {
		$post_id = $id;
	} else {
		$post_id = $post->ID;
	}
	$info = get_field( 'apk_info', $post_id );
	if ( !empty( $info ) ) {
		if ( '' == $meta ) {
			return $info;
		} else {
			switch ($meta) {
				case 'version':
				$value = $info['version'];
				break;
				case 'variant':
				$value = $info['variant'];
				break;
				case 'architecture':
				$value = $info['architecture'];
				break;
				case 'filesize':
				$value = $info['file_size'];
				break;
				case 'min_version':
				$value = $info['minimum_android_version'];
				break;
				case 'android_version':
				$value = $info['android_version'];
				break;
				case 'signatures':
				$value = $info['signatures'];
				break;
				case 'screen':
				$value = $info['dpi']['value'];
				break;
				default:
				$value = '';
				break;
			}
			return $value;
		}
	}
}

function ms_get_download_count( $id ) {
	if ( !$id ) {
		return;
	}
	$key = '_download_count';
	$count = (int) get_post_meta( $id, $key, true );
	if ( $count > 999 ) {
		$count = $count/1000;
		$count = number_format($count,2) .'K';
	}
	return $count;
}

function ms_get_file_size( $id ) {
	if ( !$id ) {
		return;
	}
	$file_size = 0;
	$q = get_posts( array(
		'post_type' => 'app_download',
		'posts_per_page' => 1,
		'meta_key' => 'release',
		'meta_value' => $id,
		'meta_compare' => 'LIKE'
	) );
	if ( $q ) {
		foreach ($q as $d) {
			$file_size = ms_get_apk_meta( 'filesize', $d->ID ) . ' MB';
		}
	}
	return $file_size;
}

function ms_paginate_links( $query, $current ) {
	$links = paginate_links( array(
		'current' => $current,
		'total'   => $query->max_num_pages
	) );

	if ( $links ) {
		?>
		<div class="page-pagination text-center">
			<div class="page-info"><?php printf( '%s %s of %s', __( "Page", "apk" ), $current, $query->max_num_pages ); ?></div>
			<div class="page-links">
				<?php echo $links; ?>
			</div>
		</div>
		<?php
	}
}

function ms_opengraph() { 
	$title = get_bloginfo( 'name' );
	$description = get_bloginfo( 'description' );;
	$image = "";
	$url = home_url( '/' );
	if ( is_singular( array( 'app_release', 'app_download', 'post', 'page' ) ) ) {
		global $post;
		$title = get_the_title( $post->ID );
		$description = get_the_excerpt( $post->ID );
		$url = get_permalink( $post->ID );

		if ( is_singular( 'app_release' ) ) {
			$post = get_queried_object();
			$obj = get_the_terms( $post->ID, 'appcategory' );
			if ( '' <> ( $app_icon = get_term_meta( $obj[0]->term_id, 'app_icon_id', true ) ) ) {
				$image = wp_get_attachment_image_url( $app_icon, 'full', true );
			}
		} else if ( is_singular( 'app_download' ) ) {
			$meta = get_post_meta( $post->ID, 'release', true );
			if ( !empty( $meta ) ) {
				$obj = get_the_terms( $meta[0], 'appcategory' );
				if ( '' <> ( $app_icon = get_term_meta( $obj[0]->term_id, 'app_icon_id', true ) ) ) {
					$image= wp_get_attachment_image_url( $app_icon, 'full', true );
				}
			}
		} else {
			$image = wp_get_attachment_image_url( get_post_thumbnail_id( $post->ID ), 'full' );
		}
	}
	?>
	<meta property="og:title" content="<?php echo $title; ?>">
	<meta property="og:description" content="<?php echo $description; ?>">
	<meta property="og:image" content="<?php echo $image; ?>">
	<meta property="og:url" content="<?php echo $url; ?>">

	<?php do_action( 'ms_opengraph' ); ?>
<?php }
add_action( 'wp_head', 'ms_opengraph' );

function ms_head_scripts() {
	$scripts = get_option( 'apk_indra_header_code' );
	if ( '' <> $scripts ) {
		echo stripslashes( htmlspecialchars_decode( $scripts ) );
	}
}
add_action( 'wp_head', 'ms_head_scripts' );

function ms_footer_scripts() {
	$scripts = get_option( 'apk_indra_footer_code' );
	if ( '' <> $scripts ) {
		echo stripslashes( htmlspecialchars_decode( $scripts ) );
	}
}
add_action( 'wp_footer', 'ms_footer_scripts' );

if ( ! function_exists( 'ms_get_menu_childrens' ) ) {
    /**
     * @param $items
     * @param int $parent
     *
     * @return array
     */
    function ms_get_menu_childrens( $items, $parent = 0 )
    {
        $bundle = [];
        foreach ( $items as $item ) {
            if ( $item->menu_item_parent == $parent ) {
                $child               = ms_get_menu_childrens( $items, $item->ID );
                $bundle[ $item->ID ] = [
                    'item'   => $item,
                    'childs' => $child
                ];
            }
        }

        return $bundle;
    }
}