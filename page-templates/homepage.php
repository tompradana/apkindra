<?php 
/**
 * Template Name: Homepage
 */
get_header(); ?>

<div class="col-12 col-lg-8 col-xl-8">

  <?php dynamic_sidebar( 'ads-72890' ); ?>

  <div id="recent-apps">

  <?php 
  $paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
  if ( is_front_page() ) {
    $paged = ( get_query_var( 'page' ) ) ? get_query_var( 'page' ) : 1;
  }
  $args  = array(
    'post_type' => 'app_release',
    'date_pagination_type' => 'daily',
    'paged' => $paged
  );
  $the_query = new WP_Query( $args );

  if ( $the_query->have_posts() ) : ?>
    
    <div class="card mb-3">
    
    <?php $i=0; while ( $the_query->have_posts() ) : $the_query->the_post(); 
      $the_date     = get_the_date( 'Y-m-d' );
      $next         = get_next_post(); 
      $next_date    = isset( $next->ID ) ? get_the_date( 'Y-m-d', $next->ID ) : '';
    ?>

      <?php if ( $i == 0 ) { ?>
        <h5 class="widget-title date"><?php echo date_i18n( 'F j', strtotime( $the_date ) ); ?></h5>
      <?php }; ?>

      <?php $i++; if ( $the_date != $next_date && $i > 1 ) { 
        ?>
        </div><!-- end .card --><div class="card mb-3"><h5 class="widget-title date"><?php echo date_i18n( 'F j', strtotime( $the_date ) ); ?></h5>  
      <?php }; ?>
      
      <?php get_template_part( 'parts/content-main', 'loop' ); ?>
      
    <?php endwhile; ?>

    <?php ms_paginate_links( $the_query, $paged ); ?>
    
    </div><!-- end .card -->

  <?php endif; wp_reset_query(); wp_reset_postdata(); ?>

  </div><!-- end #recent-apps -->
</div><!-- end col -->

<?php get_sidebar(); ?>

<?php get_footer(); ?>