<?php
/**
 * Template Name: Categories
 */
get_header(); ?>

<div class="col-12 col-lg-8 col-xl-8">
	
  <div id="app-categories">
    <div class="card mb-3">
      <h5 class="widget-title date">
        <?php _e( 'All Categories', 'apk' ); ?>
      </h5>

      <?php 
      	$categories = get_terms( array( 'taxonomy' => 'category', 'hide_empty' => 1 ) ); 
      	foreach( $categories as $cat ) : 
      		$id = uniqid( $cat->slug );
      ?>
	      <div class="card-header" id="<?php echo $cat->slug; ?>">
	        <div class="mb-0 d-flex align-items-center justify-content-between">
	          <a href="<?php echo get_term_link( $cat ); ?>"><?php echo $cat->name; ?></a>
	          <a href="#" data-toggle="collapse" data-target="#<?php echo $id; ?>" aria-expanded="true" aria-controls="<?php echo $id; ?>"><i class="material-icons md-24">arrow_drop_down</i></a>
	        </div>
	      </div>
	      <div id="<?php echo $id; ?>" class="collapse" aria-labelledby="<?php echo $cat->slug; ?>" data-parent="#accordion">
	        <div class="card-body">
	          <?php
	          	$applications = get_terms( array(
	          		'taxonomy' 		=> 'appcategory',
	          		'number'			=> 5,
	          		'meta_query'	=> array(
	          			array(
	          				'key' 		=> 'appcat_id',
	          				'value'		=> $cat->term_id,
	          				'compare' => '='
	          			)
	          		)
	          	) );
	          	if ( !empty( $applications ) ) {
	          		echo '<div class="app-table">';
	          		foreach( $applications as $app ) { 
	          		$icon_img = '';
								$app_icon = get_term_meta( $app->term_id, 'app_icon_id', true );
								if ( $app_icon ) {
									$icon_img = wp_get_attachment_image( $app_icon, array(32,32), true );
								}
	          		?>
	          			<div class="app-apk app-row">
	          				<div class="icon" style="width: 56px"><?php echo $icon_img; ?></div>
	          				<div class="app-name app-title">
	          					<h5 class="title"><a href="<?php echo get_term_link( $app ); ?>"><?php echo $app->name; ?></a></h5>
	          				</div>
	          				<div class="app-info-download">
									    <a href="<?php echo get_term_link( $app ); ?>"><i class="material-icons">file_download</i></a>
									  </div>
	          			</div>
	          		<?php }
	          		echo '</div>';
	          	}
	          ?>
	          <div class="text-center mt-3">
	          	<a href="<?php echo get_term_link( $cat ); ?>"><?php printf( __( 'See more %s apps...', 'apk' ), $cat->name ); ?></a>
	          </div>
	        </div>
	      </div>
    	<?php endforeach; ?>

    </div>
  </div>
</div>

<?php get_sidebar(); ?>

<?php get_footer(); ?>
