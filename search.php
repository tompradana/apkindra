<?php 
$searchtype = isset( $_GET['searchtype'] ) ? $_GET['searchtype'] : '';
$squery = urlencode( get_search_query() );
get_header(); ?>

<div class="col-12 col-lg-8 col-xl-8">

	<?php dynamic_sidebar( 'ads-72890' ); ?>

	<div id="recent-apps">

		<div class="card-with-tabs mb-3">
			<h5 class="widget-title"><?php printf( __( 'Results for "%s"', 'apk' ), get_search_query() ); ?></h5>

			<div class="tab-container">
				<div class="tab-scrollable-container">
					<ul class="nav nav-tabs" id="myTab" role="tablist">
						<li class="nav-item">
							<a class="nav-link <?php if ( $searchtype == 'apk' || $searchtype == '' ) { ?>active<?php } ?>" href="<?php echo add_query_arg( array( 'searchtype' => 'apk', 's' => $squery ), home_url('/') ); ?>"><?php _e( 'APKS', 'apk' ); ?></a>
						</li>
						<li class="nav-item">
							<a class="nav-link <?php if ( $searchtype == 'app' ) { ?>active<?php } ?>" href="<?php echo add_query_arg( array( 'searchtype' => 'app', 's' => $squery ), home_url('/') ); ?>"><?php _e( 'APPS', 'apk' ); ?></a>
						</li>
						<li class="nav-item">
							<a class="nav-link <?php if ( $searchtype == 'dev' ) { ?>active<?php } ?>" href="<?php echo add_query_arg( array( 'searchtype' => 'dev', 's' => $squery ), home_url('/') ); ?>"><?php _e( 'DEVS', 'apk' ); ?></a>
						</li>
					</ul>
				</div>
			</div>
			<!-- end .tab-container -->

			<div class="tab-content" id="myTabContent">
				<div class="tab-pane fade show active">
					<?php
					if ( $searchtype == 'apk' || $searchtype == '' ) :
						$args = array(
							'post_type'       => 'app_release',
							'posts_per_page'  => 10,
							's'               => get_search_query()
						);
						$custom_query = new WP_Query( $args ); ?>
						<?php if ( $custom_query->have_posts() ) : ?>
							<?php while ( $custom_query->have_posts() ) : $custom_query->the_post(); ?>
								<?php get_template_part( 'parts/content-search', 'loop' ); ?>
							<?php endwhile; ?>
							<?php else : ?>
								<div class="pt-3 pb-3 pl-3 pr-3 text-center"><?php _e( 'No results found matching your query', 'apk' ); ?></div>
							<?php endif; ?>
							<?php wp_reset_postdata(); ?>
							<?php elseif ( $searchtype == 'app' ) : ?>
								<?php $applications = get_terms( array( 
									'taxonomy'    => 'appcategory',
									'number'      => 10,
									'name__like'  => get_search_query(),
								) );
								if ( !empty( $applications ) ) {
									foreach( $applications as $app ) { 
										$icon_img = '';
										$app_icon = get_term_meta( $app->term_id, 'app_icon_id', true );
										if ( $app_icon ) {
											$icon_img = wp_get_attachment_image( $app_icon, array(32,32), true );
										}
										$release = array(
											'post_type' => 'app_release',
											'posts_per_page' => 1,
											'tax_query' => array(
												array(
													'taxonomy' => 'appcategory',
													'terms' => array( $app->term_id )
												)
											)
										);
										$has_release = false;
										$post_release_id = false;
										$rq = new WP_Query( $release );
										if ( $rq->have_posts() ) {
											$has_release = true;
											while ( $rq->have_posts() ) : $rq->the_post();
												$post_release_id = get_the_ID();
											endwhile;
										}
										wp_reset_postdata();
										?>
										<div class="app-table">
											<div class="app-apk app-row">
												<div class="icon" style="width: 56px"><?php echo $icon_img; ?></div>
												<div class="app-name app-title">
													<h5 class="title"><a href="<?php echo get_term_link( $app ); ?>"><?php echo $app->name; ?></a></h5>
												</div>
												<div class="app-info-download">
													<?php if ( $has_release ) : ?>
														<a class="view-detail" href="#"><i class="material-icons">info</i></a>
													<?php endif; ?>
													<a href="<?php the_permalink(); ?>"><i class="material-icons">file_download</i></a>
												</div>
											</div>
										</div>

										<?php
										if ( $post_release_id !== false ) :
											$args  = array(
												'post_type'       => 'app_download',
												'posts_per_page'  => 1,
												'order'           => 'ASC',
												'meta_key'        => 'release',
												'meta_value'      => $post_release_id,
												'meta_compare'    => 'LIKE'
											); ?>
											<?php $apkq = new WP_Query( $args ); ?>
											<?php if ( $apkq->have_posts() ) : ?>
												<?php while ( $apkq->have_posts() ) : $apkq->the_post(); ?>
													<div class="infoSlide">
														<p><span class="infoslide-name">Version</span><span class="infoslide-value"><?php echo ms_get_apk_meta( 'version' ); ?></span></p>
														<p><span class="infoslide-name">Uploaded</span><span class="infoslide-value"><?php if ( is_page_template( 'page-templates/homepage.php' ) ) : ?><?php echo esc_html( human_time_diff( get_the_time('U'), current_time('timestamp') ) ) . ' ago'; ?><?php else : ?><?php echo printf( __( '%s at %s', 'apk' ), get_the_date( 'F j, Y '), get_the_date( 'g:iA T' ) ); ?><?php endif; ?></span>
														</p>
														<p><span class="infoslide-name">File Size</span><span class="infoslide-value"><?php echo ms_get_apk_meta( 'filesize' ); ?></span></p>
														<p><span class="infoslide-name">Downloads</span><span class="infoslide-value"><?php echo ms_get_download_count(get_the_ID()); ?></span></p>
													</div>
												<?php endwhile; ?>
											<?php endif; ?>
											<?php wp_reset_postdata(); ?>
										<?php endif; ?>
									<?php }
								} else { ?>
									<div class="pt-3 pb-3 pl-3 pr-3 text-center"><?php _e( 'No results found matching your query', 'apk' ); ?></div>
								<?php } ?>
								<?php else : ?>
									<?php $applications = get_terms( array( 
										'taxonomy'    => 'devcategory',
										'number'      => 10,
										'name__like'  => get_search_query(),
									) );
									if ( !empty( $applications ) ) {
										foreach( $applications as $app ) { 
											$icon_img = '';
											$app_icon = get_term_meta( $app->term_id, 'app_icon_id', true );
											if ( $app_icon ) {
												$icon_img = wp_get_attachment_image( $app_icon, array(32,32), true );
											}
											?>
											<div class="app-table">
												<div class="app-apk app-row">
													<div class="app-name app-title">
														<h5 class="title"><a href="<?php echo get_term_link( $app ); ?>"><?php echo $app->name; ?></a></h5>
													</div>
													<div class="app-info-download">
														<a href="<?php echo get_term_link( $app ); ?>"><i class="material-icons">file_download</i></a>
													</div>
												</div>
											</div>
										<?php }
									} else { ?>
										<div class="pt-3 pb-3 pl-3 pr-3 text-center"><?php _e( 'No results found matching your query', 'apk' ); ?></div>
									<?php } ?>
								<?php endif; ?>
							</div>
							
						</div>
					</div><!-- end .card -->
				</div>

				<?php dynamic_sidebar( 'ads-72890-bottom' ); ?>

			</div>

			<?php get_sidebar(); ?>
			<?php get_footer(); ?>