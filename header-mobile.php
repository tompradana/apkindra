<div class="mobile-search-form d-none">
	<form class="searchform-box" action="<?php echo home_url('/'); ?>" method="get" accept-charset="utf-8">
		<button class="searchButton">
			<i class="material-icons">search</i>
		</button>
		<input type="text" name="s" class="searchbox" value="<?php echo get_search_query(); ?>" placeholder="<?php esc_attr_e( 'Search', 'apk' ); ?>">
	</form>
	<a class="close-form">&times;</a>
</div>
<div class="header-mobile d-flex align-items-center justify-content-between">
	<div class="mobile-menu d-flex align-items-center">
		<div onclick="openNav()" class="mobile-menu-btn"><span></span></div>
		<div class="brand"><a href="<?php echo home_url('/'); ?>"><?php echo get_bloginfo(); ?></a></div>
	</div>
	<div class="mobile-search">
		<a class="search-btn">
			<i class="material-icons">search</i>
		</a>
		<a class="upload-icon" href="#"><i class="material-icons">backup</i></a>
	</div>
</div>