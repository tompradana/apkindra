<?php get_header(); 
while ( have_posts() ) : the_post();
	$post     = get_queried_object();
	$taxonomy = 'appcategory';
	$obj      = get_the_terms( $post->ID, $taxonomy );
	if ( ! is_wp_error( $obj ) && !empty( $obj ) ) {
		$obj = $obj[0];
	}
	?>

	<div class="col-12 col-lg-8 col-xl-8">
		<div id="app-tabs">

			<?php dynamic_sidebar( 'ads-72890' ); ?>

			<div class="card-with-tabs mb-3">

				<div class="tab-container">
					<div class="tab-scrollable-container">
						<ul class="nav nav-tabs" id="myTab" role="tablist">
							<li class="nav-item">
								<a class="nav-link active" data-toggle="tab" href="#changelog-tab" role="tab" aria-controls="changelog-tab" aria-selected="false"><?php _e( 'What\'s New', 'apk' ); ?></a>
							</li>
							<li class="nav-item">
								<a class="nav-link" data-toggle="tab" href="#description-tab" role="tab" aria-controls="description-tab" aria-selected="true"><?php _e( 'Descriptions', 'apk' ); ?></a>
							</li>
						<!-- <li class="nav-item">
							<a class="nav-link" data-toggle="tab" href="#file-tab" role="tab" aria-controls="file-tab" aria-selected="true"><?php _e( 'File', 'apk' ); ?></a>
						</li> -->
					</ul>
				</div>
			</div>
			<!-- end .tab-container -->

			<div class="tab-buttons d-flex align-items-center justify-content-between">
				<div class="app-meta">
					<div class="app-category"><?php the_category(); ?></div>
					<div class="app-rating d-flex align-items-center">
						<?php ms_rating( $obj->term_id ); ?>
						<span><?php printf( __( '(%s ratings)', 'apk' ), number_format( (float) get_term_meta( $obj->term_id, 'total_rating', true ) ) ); ?></span>
					</div>
				</div>
				<?php include( locate_template( 'parts/sharer.php' ) ); ?>
			</div>

			<div class="tab-content" id="myTabContent">
				<div class="tab-pane fade show active" id="changelog-tab" role="tabpanel" aria-labelledby="changelog-tab">
					<div class="entry-content">
						<?php the_content( ); ?>
					</div>
				</div>
				<div class="tab-pane fade" id="description-tab" role="tabpanel" aria-labelledby="description-tab">
					<?php 
					$string = $obj->description;
						// $email  = '/(\S+@\S+\.\S+)/';
						// $string = preg_replace($email, '<a href="mailto:$1">$1</a>', $string );
						// $url    = '@(http(s)?)?(://)?(([a-zA-Z])([-\w]+\.)+([^\s\.]+[^\s]*)+[^,.\s])@';
						// $string = preg_replace($url, '<a href="http$2://$4" target="_blank" title="$0">$0</a>', $string );
					echo wpautop( make_clickable( $string ) ); 
					?>
				</div>
				<!-- <div class="tab-pane fade" id="file-tab" role="tabpanel" aria-labelledby="file-tab">
					<?php 
						var_dump( get_field( 'apk_info' ) );
					?>
				</div> -->
			</div>

			<div class="view-all-apk d-flex justify-content-center pb-4">
				<div class="col-12 col-lg-6 col-xl-6">
					<a class="download-apk-btn btn btn-raised btn-primary btn-block" href="#downloads"><i class="material-icons">file_download</i><?php _e( 'See Available APK', 'apk' ); ?></a>
				</div>
			</div>

		</div><!-- end .card -->   
	</div><!-- end #recent-apps -->

	<?php dynamic_sidebar( 'ads-release_1-72890' ); ?>

	<div id="downloads">
		<div class="card mb-3">
			<h5 class="widget-title date"><?php _e( 'Download', 'apk'); ?></h5>
			<div class="apk-downloads">
				<div class="table-responsive">
					<table class="table">
						<thead>
							<tr>
								<th scope="col"><?php _e( 'Variant', 'apk' ); ?></th>
								<th scope="col"><?php _e( 'Architecture', 'apk' ); ?></th>
								<th scope="col"><?php _e( 'Minimum Version', 'apk' ); ?></th>
								<th scope="col"><?php _e( 'Screen DPI', 'apk' ); ?></th>
							</tr>
						</thead>
						<tbody>
							<?php
							$args  = array(
								'post_type'       => 'app_download',
								'posts_per_page'  => -1,
								'order'           => 'ASC',
								'meta_key'        => 'release',
								'meta_value'      => get_the_ID(),
								'meta_compare'    => 'LIKE'
							);
							$query = new WP_Query( $args );
							if ( $query->have_posts() ) : ?>
								<?php while ( $query->have_posts() ) : $query->the_post(); ?>
									<tr>
										<th scope="row">
											<a href="<?php the_permalink(); ?>"><i class="material-icons">label</i><?php echo ms_get_apk_meta( 'variant' ); ?></a><span class="badge badge-success">APK</span><br/>
											<span class="apk-download-date"><?php echo get_the_date( 'j F Y' ); ?></span>
										</th>
										<td><?php echo ms_get_apk_meta( 'architecture' ); ?></td>
										<td><?php echo ms_get_apk_meta( 'min_version' ); ?></td>
										<td><?php echo ms_get_apk_meta( 'screen' ); ?></td>
									</tr>
								<?php endwhile; ?>
							<?php endif; wp_reset_postdata(); ?>
						</tbody>
					</table>
				</div>
			</div>
		</div><!-- end .card -->
	</div>

	<?php dynamic_sidebar( 'ads-release_2-72890' ); ?>
	
	<div id="all-releases">
		<div class="card mb-3">
			<h5 class="widget-title date"><?php _e( 'All Releases', 'apk'); ?></h5>
			<?php
			$args  = array(
				'post_type' => array( 'app_release' ),
				'date_pagination_type' => 'daily',
				'posts_per_page' => 10,
				'tax_query' => array(
					array(
						'taxonomy' => 'appcategory',
						'terms' => array( $obj->term_id )
					)
				)
			);
			$query = new WP_Query( $args );
			if ( $query->have_posts() ) : ?>
				<?php while ( $query->have_posts() ) : $query->the_post(); ?>
					<?php get_template_part( 'parts/content-allrelease', 'loop' ); ?>
				<?php endwhile; ?>
			<?php endif; wp_reset_postdata(); ?>
		</div><!-- end .card -->
	</div>

	<div class="card mt-3">
		<h5 class="widget-title date"><?php _e( 'Comments', 'apk' ); ?></h5>
		<div class="disqus-comments-wraper">
			<?php comments_template(); ?>
		</div>
	</div>

</div><!-- end col -->

<?php endwhile; ?>

<?php get_sidebar(); ?>

<?php get_footer(); ?>