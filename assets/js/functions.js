(function($) {
    $(document).ready(function() {
        // init material bs
        $('body').bootstrapMaterialDesign();
        $('#mySidenav ul.menu > li.menu-item-has-children').each(function(e) {
            $(this).find('>a').addClass('dropdown-toggle');
        });

        $('#mySidenav a.dropdown-toggle').on('click', function(e) {
            var $this = $(this);
            if ($(this).hasClass('opened')) {
                if ($(this).attr('href').indexOf("#") != -1) {
                	e.preventDefault();
                    $(this).next('.sub-menu').slideUp('slow/400/fast', function() {
                    	$this.removeClass('opened');
                    });
                } else {
                    return true;
                }
            } else {
                e.preventDefault();
                $(this).addClass('opened');
                $(this).next('.sub-menu').slideDown('slow/400/fast', function() {});
            }
        });
    });

    $('.close-form').on('click', function() {
        $(this).parent('div').addClass('d-none');
    });
    $('.mobile-search > a').on('click', function() {
        $('.mobile-search-form').removeClass('d-none');
        $('.mobile-search-form').find('input[type="text"]').focus();
    });

    $('.app-info-download .view-detail').on('click', function(e) {
        console.log(e);
        e.preventDefault();
        var $this_p = $(this).parents('.app-table');
        if ($(this).hasClass('active')) {
            $(this).removeClass('active');
            $this_p.next('.infoSlide').slideUp('fast').removeClass('active');
        } else {
            $('.app-info-download .view-detail').removeClass('active');
            $('.infoSlide').slideUp('fast').removeClass('active');
            $(this).addClass('active');
            $this_p.next('.infoSlide').slideDown('fast').addClass('active');
        }
    });



})(jQuery);

function openNav() {
    document.getElementById("mySidenav").style.left = "0";
    document.body.classList.add("sidenavopen");
}

/* Set the width of the side navigation to 0 and the left margin of the page content to 0, and the background color of body to white */
function closeNav() {
    document.getElementById("mySidenav").style.left = "-100%";
    document.body.classList.remove("sidenavopen");
}