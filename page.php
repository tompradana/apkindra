<?php get_header(); ?>

<div class="col-12 col-lg-8 col-xl-8">

	<?php dynamic_sidebar( 'ads-72890' ); ?>
	
	<?php while ( have_posts() ) : the_post(); ?>

  <div id="page-<?php the_ID(); ?>" class="page-content">
    <div class="card mb-3">
			<div class="article-inside">
				<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
					<header class="entry-header">
						<h1 class="entry-title"><?php the_title(); ?></h1>
					</header>
					<div class="entry-content">
						<?php the_content(); ?>
					</div>
				</article>
			</div>
    </div><!-- end .card -->
  </div><!-- end #recent-apps -->

	<?php endwhile; ?>

	<?php dynamic_sidebar( 'ads-72890-bottom' ); ?>

</div><!-- end col -->

<?php get_sidebar(); ?>

<?php get_footer(); ?>