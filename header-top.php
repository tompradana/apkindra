<?php if ( is_singular( array( 'app_download', 'app_release' ) ) || is_tax( 'appcategory' ) ) : ?>
<div id="site-header-top">
	<div class="container">
		<div class="d-block d-lg-none d-xl-none">
		  <?php get_header( 'mobile' ); ?>
		</div>
		<div class="d-none d-lg-block d-xl-block">
			<div class="row d-flex align-items-center">
				<div class="col-sm-8">
					<a class="text-logo" href="<?php echo home_url( '/' ); ?>"><?php bloginfo( 'name' ); ?></a>
				</div>
				<?php get_search_form(); ?>
			</div>
		</div>
	</div>
</div>
<?php endif; ?>