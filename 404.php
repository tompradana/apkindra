<?php get_header(); 
	global $wp;
?>

<div class="col-12 col-lg-8 col-xl-8">

	<?php dynamic_sidebar( 'ads-72890' ); ?>

  <div id="404-error">
    <div class="card mb-3">
    	<div class="table-row">
				<a href="<?php echo home_url( '/' ); ?>" class="d-flex pt-5 pb-5 pr-5 pl-5">
					<div class="errorLeft mr-5">
						<h1>404</h1>
						<h5>Whoops!</h5>
					</div>
					<div class="errorRight">
						The requested URL <span style="color:orange"><?php echo home_url( add_query_arg( array(), $wp->request ) ); ?></span> could not be found. <br> <br> Hit the back button or click this message to zoom back to the home page.
					</div>
				</a>
			</div>
    </div>
  </div>

  <?php dynamic_sidebar( 'ads-72890-bottom' ); ?>

</div>

<?php get_sidebar(); ?>

<?php get_footer(); ?>