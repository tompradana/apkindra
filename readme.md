### Cara Instalasi
1. Install theme melalui FTP > wp-content > themes atau Admin > Appearance > Themes > Upload
2. Aktivasi theme
3. Install plugin ACF dan Classic editor dan aktifkan
4. Import ACF Fields (apk/themes/import_files/acf.json) melalui Admin > Custom Fields > Tools > Import
5. Reset permalink Admin > Settings > Permalink > Save

### Page Template
Theme ini memiliki beberapa page template:
- homepage.php untuk menampilkan recent releases
- categories.php untuk menampilkan release berdasarakan urutan kategori
- developers.php untuk menampilkan release berdasarkan urutan developer
- uploads.php (ongoing)