<?php get_header(); 
$obj = get_queried_object();
?>

<div class="col-12 col-lg-8 col-xl-8">
	<div id="developer-tabs">
		<div class="card-with-tabs mb-3">
			<div class="tab-container">
				<div class="tab-scrollable-container">
					<ul class="nav nav-tabs" id="myTab" role="tablist">
						<li class="nav-item">
							<a class="nav-link active" data-toggle="tab" href="#description-tab" role="tab" aria-controls="description-tab" aria-selected="true"><?php _e( 'Descriptions', 'apk' ); ?></a>
						</li>
						<li class="nav-item">
							<a class="nav-link" data-toggle="tab" href="#changelog-tab" role="tab" aria-controls="changelog-tab" aria-selected="false"><?php _e( 'What\'s New', 'apk' ); ?></a>
						</li>
					</ul>
				</div>
			</div>
			<!-- end .tab-container -->

			<div class="tab-buttons d-flex align-items-center justify-content-between">
				<div class="app-meta">
					<div class="app-category"><?php the_category(); ?></div>
					<div class="app-rating d-flex align-items-center">
						<?php ms_rating( $obj->term_id ); ?>
						<span><?php printf( __( '(%s ratings)', 'apk' ), number_format( (float) get_term_meta( $obj->term_id, 'total_rating', true ) ) ); ?></span>
					</div>
				</div>
				<?php include( locate_template( 'parts/sharer.php' ) ); ?>
			</div>

			<div class="tab-content" id="myTabContent">
				<div class="tab-pane fade show active" id="description-tab" role="tabpanel" aria-labelledby="description-tab">
					<?php 
					$string = $obj->description;
						// $email  = '/(\S+@\S+\.\S+)/';
						// $string = preg_replace($email, '<a href="mailto:$1">$1</a>', $string );
						// $url    = '@(http(s)?)?(://)?(([a-zA-Z])([-\w]+\.)+([^\s\.]+[^\s]*)+[^,.\s])@';
						// $string = preg_replace($url, '<a href="http$2://$4" target="_blank" title="$0">$0</a>', $string );
					echo wpautop( make_clickable( $string ) ); 
					?>
				</div>
				<div class="tab-pane fade" id="changelog-tab" role="tabpanel" aria-labelledby="changelog-tab">
					<?php $qquery = new WP_Query( array(
						'post_type' => 'app_release',
						'posts_per_page' => 1,
						'tax_query' => array(
							array(
								'taxonomy' => 'appcategory',
								'terms' => array( $obj->term_id )
							)
						)
					) ); ?>
					<?php if ( $qquery->have_posts() ) : while ( $qquery->have_posts() ) : $qquery->the_post(); ?>
						<?php the_content(); ?>
					<?php endwhile; endif; ?>
					<?php wp_reset_postdata(); ?>
				</div>
			</div>
		</div><!-- end .card -->
	</div><!-- end #recent-apps -->

	<div id="all-versions">
		<div class="card mb-3">
			<h5 class="widget-title date"><?php _e( 'All Versions', 'apk'); ?></h5>
			<?php
			$args  = array(
				'post_type' => array( 'app_release' ),
				'date_pagination_type' => 'daily',
				'posts_per_page' => 10,
				'tax_query' => array(
					array(
						'taxonomy' => 'appcategory',
						'terms' => array( $obj->term_id )
					)
				)
			);
			$query = new WP_Query( $args );
			if ( $query->have_posts() ) : ?>
				<?php while ( $query->have_posts() ) : $query->the_post(); ?>
					<?php get_template_part( 'parts/content-appcategory', 'loop' ); ?>
				<?php endwhile; ?>
			<?php endif; wp_reset_postdata(); ?>
		</div><!-- end .card -->
	</div>
</div><!-- end col -->

<?php get_sidebar(); ?>

<?php get_footer(); ?>