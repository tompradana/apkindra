<div class="col-lg-4 d-none d-lg-block">
	<div class="d-flex justify-content-between align-items-center">
		<form class="searchform-box" action="<?php echo home_url('/'); ?>" method="get" accept-charset="utf-8">
			<button type="submit" class="searchButton">
				<i class="material-icons">search</i>
			</button>
			<input type="text" name="s" class="searchbox" value="<?php echo get_search_query(); ?>" placeholder="<?php esc_attr_e( 'Search', 'apk' ); ?>">
		</form>
		<a class="upload-icon" href="#"><i class="material-icons">backup</i></a>
	</div>
</div>